#
# Signal (PWM) for buzzer control.
# When signal is HIGH, buzzer will generate sound. 
# For example if signal period is 1 second and duty cycle 50% then the buzzer
# will sound for 0.5 seconds and will be OFF for 0.5 seconds then is repeating
# 
# An counter is incremented at 80ms (task reccurence). Signal is generated based
# on this clock. Below counts is reffering to this counter value.
# 


# INPUTS:
# Period of PWM signal in seconds
pwm_period = 0.5
# PWM duty cycle, in %
duty_cycle = 25
# PWM clock in seconds (PWM is based on this clock input)
pwm_clock = 0.08

#
# PWM frequency (not used, just calculated)
pwm_freq = 1 / pwm_period

# Signal ON time, in seconds
Ton = (duty_cycle / 100) * pwm_period
# Signal OFF time, in seconds
Toff = pwm_period - Ton

# Signal ON time, in counts
Ton_counts = round(Ton / pwm_clock)
# Signal OFF time, in counts
Toff_counts = round(Toff / pwm_clock)




