# README #



### Project ###
Small project showing how to read temperature from Papouch TQS3 thermometer and display temperature on
a 4 digits 7 segment display from Mikroelectronica (MIKROE-201: Serial 7-seg Display Board MAX7219).
Above operations are controlled by a STM32F103 microcontroller, on a OLIMEXINO STM32 board.  
All things are connected together using a prototyping board - *Base board* in System diagram.

### System IO ###
 - UART 1 is connected to RS485 transceiver, communication bus for temperature sensors.
 - SPI 2 use to send data to display (MAX7219). 4 digits, 7 segment - display temperature sensor number and temperature value (Celsius degrees)
 - Relay output:  is used to control an relay (Omron G5RL-1A-E-HR 16A/250VAC)
 - Buzzer output (PC8) is used for turning buzzer on/off. Alarm when temperature is out of range 
 - LED output (PB7), flash when temperature limits monitoring is disabled 
 - Switch input (PA0) disable temperature limits monitoring



Links to boards used in project:  
[Olimex OLIMEXINO-STM32](https://www.olimex.com/Products/Duino/STM32/OLIMEXINO-STM32/open-source-hardware)  
[Olimex RS485 transciever MOD-RS485](https://www.olimex.com/Products/Modules/Interface/MOD-RS485/open-source-hardware)  
[Papouch TQS3 thermometer](https://www.papouch.com/en/shop/product/tqs3-o-outdoor-rs485-thermometer/)  
[ MIKROE-201: Serial 7-seg Display Board](https://shop.mikroe.com/serial-7-seg-display-board) 


### System diagram ###
Open image in new tab to enlarge
![System diagram](diagrams/system_diagram.png)
Diagram was drawn using Draw.io online diagram software. File is saved in *diagrams* folder (system\_diagram.xml file).

### IDE ###
[VisualGDB](https://visualgdb.com/?features=embedded)  
IDE: Visual Studio  
Toolchain: GCC for ARM  
STM32 library: STM32CubeF1  
IDE, toolchain and debug settup is described [here](https://visualgdb.com/tutorials/arm/stm32/)

[STM32CubeMX](http://www.st.com/en/development-tools/stm32cubemx.html) application was used to generate C initialization code (see olimexino_stm32.ioc file).
Link to this tool is at the end of the ST page - GET SOFTWARE section.


### Project structure and folders ###
Project root folder contains below folders. Most of them are explained below.  
![Project structure](pics/directory_top.PNG)  
Code and Visual Studio project is in *project* directory. Below is the structure of this folder:  
![Project structure](pics/sw_proj.PNG)  
Folders *Inc* and *Src* contains C files generated using CubeMX. CubeMX project was saved in olimexino\_stm32.ioc file. Below is a screen shoot of CubeMX project. What is green is used in this project.  
![Project structure](pics/cubemx_olimexino-stm32.PNG)  
Initialization code need to be regenerated only when pins and/or microcontroller peripherals has changed.  

Visual Studio project should look as below. To successfully open the project VisualGDB should be installed, GCC toolchain should be present and Embedded BSP for STM32 should be downloaded.  
VisualGDB will ask to download the toolchain and necessary Embedded BSP if you try to open the project (temperature\_monitor.sln) and above are missing.

**Very Important!**  
Visual GDB use systems Users folder for shared files (Embedded BSP). This location will be different from what is now in project. To update for new location use *Regenerate MCU files* button from *VisualGDB Project Properties* - see below picture.  
![Regenerate MCU files](pics/Regenerate_MCU_files.PNG)

When everything is set correctly, in Visual Studio, Solution Explorer view should looks like below:  
![VS Project view](pics/vs_project_explorer.PNG)


### Assembly view ###
![Assembly view](pics/assembly_view.jpg)


### Programming and debug ###
For programming and debug J-Link Edu probe was used with additional header from Olimex - see below picture

![Programming and debug](pics/flash_and_debug.png) 

### Schematic ###
Schematic for the base board (this board is used to route signals from/to other boards and adds I/Os) is in "hardware" directory. A screen-shoot is below.  
![Schematic](pics/base_board_schematic.PNG)


### Video ###
A short video on YouTube
[Youtube Video](https://youtu.be/JDdN9ZrMRo0)  


### Software License ###

The software is licensed under MIT License (this license apply to modules contained in "sources" directory. Generated files has a license similar with this license - see generated files license section).

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

### Who do I talk to? ###
* cipriansalageanu@gmail.com