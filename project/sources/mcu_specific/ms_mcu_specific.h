#ifndef __MS_MCU_SPECIFIC_H
#define __MS_MCU_SPECIFIC_H

#include "bsp.h"

#ifdef __cplusplus
 extern "C" {
#endif

///////////////////////////////////////////////////////////////////////////////
// Public types definitions
///////////////////////////////////////////////////////////////////////////////
typedef uint32_t base_type_t;


///////////////////////////////////////////////////////////////////////////////
// Global variables
///////////////////////////////////////////////////////////////////////////////
/* Count how mnay times interrupts has been disabled. */
volatile base_type_t i_cnt;


//******************************************************************************
// Disable all interrupts
//******************************************************************************
__attribute__( ( always_inline ) ) static inline void ms_disable_irq(void)
{
    __disable_irq();
    // Increment interrupts disabled counter
    i_cnt++;
}

//******************************************************************************
// Enable all interrupts 
//******************************************************************************
__attribute__( ( always_inline ) ) static inline void ms_enable_irq(void)
{
    // Decrement interrupts disable counter
    if (i_cnt)
    {
        i_cnt--;
    }

    if (i_cnt == 0)
    {
        __enable_irq();
    }
}

#endif
