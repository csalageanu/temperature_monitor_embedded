/**
  ******************************************************************************
  * file    bsp.h
  * date    19-Jan-2018
  ******************************************************************************
  */

#ifndef __BSP_H
#define __BSP_H

#include <stdbool.h>

// Microcontroller specific header.
// Do not include microcontroller headers in BSP upper layers
#include "stm32f1xx_hal.h"
#include "usart.h"
#include "tim.h"
#include  "spi.h"


#ifdef __cplusplus
 extern "C" {
#endif

///////////////////////////////////////////////////////////////////////////////
// Types definitions
///////////////////////////////////////////////////////////////////////////////

// UART status
enum BSP_UART_STATUS
{
    UART_READY,
    UART_BUSY
};



//==========================================
// Configuration for STM32F4DISCOVERY board
//==========================================


///////////////////////////////////////////////////////////////////////////////
// LED 1 on board
///////////////////////////////////////////////////////////////////////////////
#define bps_LED1_Toggle()    HAL_GPIO_TogglePin(LED_1_GPIO_Port, LED_1_Pin)
#define bsp_LED1_On()        HAL_GPIO_WritePin(LED_1_GPIO_Port, LED_1_Pin, GPIO_PIN_SET)
#define bsp_LED1_Off()       HAL_GPIO_WritePin(LED_1_GPIO_Port, LED_1_Pin, GPIO_PIN_RESET)


///////////////////////////////////////////////////////////////////////////////
// Error LED, LED 2 on board (YELLOW)
///////////////////////////////////////////////////////////////////////////////
#define bsp_Error_LED_ON    HAL_GPIO_WritePin(LED_2_GPIO_Port, LED_2_Pin, GPIO_PIN_SET)
#define bsp_Error_LED_OFF   HAL_GPIO_WritePin(LED_2_GPIO_Port, LED_2_Pin, GPIO_PIN_RESET)


///////////////////////////////////////////////////////////////////////////////
// TX enable line for RS48 transceiver chip
///////////////////////////////////////////////////////////////////////////////
#define bsp_485_TX_On()     HAL_GPIO_WritePin(RS485_nRE_DE_GPIO_Port, RS485_nRE_DE_Pin, GPIO_PIN_SET)
#define bsp_485_TX_Off()    HAL_GPIO_WritePin(RS485_nRE_DE_GPIO_Port, RS485_nRE_DE_Pin, GPIO_PIN_RESET)

#define bsp_max7219_CS_High()    HAL_GPIO_WritePin(Displ_nCS_GPIO_Port, Displ_nCS_Pin, GPIO_PIN_SET)
#define bsp_max7218_CS_Low()     HAL_GPIO_WritePin(Displ_nCS_GPIO_Port, Displ_nCS_Pin, GPIO_PIN_RESET)


///////////////////////////////////////////////////////////////////////////////
// Digital output DO_0
///////////////////////////////////////////////////////////////////////////////
#define bps_DO_Relay_Toggle()    HAL_GPIO_TogglePin(DO_0_GPIO_Port, DO_0_Pin)
#define bsp_DO_Relay_On()        HAL_GPIO_WritePin(DO_0_GPIO_Port, DO_0_Pin, GPIO_PIN_SET)
#define bsp_DO_Relay_Off()       HAL_GPIO_WritePin(DO_0_GPIO_Port, DO_0_Pin, GPIO_PIN_RESET)

///////////////////////////////////////////////////////////////////////////////
// Digital output DO_1
///////////////////////////////////////////////////////////////////////////////
#define bps_DO_Buzzer_Toggle()    HAL_GPIO_TogglePin(DO_1_GPIO_Port, DO_1_Pin)
#define bsp_DO_Buzzer_On()        HAL_GPIO_WritePin(DO_1_GPIO_Port, DO_1_Pin, GPIO_PIN_SET)
#define bsp_DO_Buzzer_Off()       HAL_GPIO_WritePin(DO_1_GPIO_Port, DO_1_Pin, GPIO_PIN_RESET)

///////////////////////////////////////////////////////////////////////////////
// Digital output DO_2 (LED)
///////////////////////////////////////////////////////////////////////////////
#define bps_DO_Temperature_Alarm_Disabled_Toggle()    HAL_GPIO_TogglePin(DO_2_GPIO_Port, DO_2_Pin)
#define bsp_DO_Temperature_Alarm_Disabled_On()        HAL_GPIO_WritePin(DO_2_GPIO_Port, DO_2_Pin, GPIO_PIN_SET)
#define bsp_DO_Temperature_Alarm_Disabled_Off()       HAL_GPIO_WritePin(DO_2_GPIO_Port, DO_2_Pin, GPIO_PIN_RESET)


///////////////////////////////////////////////////////////////////////////////
// Button on board
///////////////////////////////////////////////////////////////////////////////
#define bsp_DI_Temperature_Alarm_Disable_Read()     (!HAL_GPIO_ReadPin(DI_0_GPIO_Port, DI_0_Pin))


///////////////////////////////////////////////////////////////////////////////
// UART 1 provided interface for upper layers
///////////////////////////////////////////////////////////////////////////////

// Not used - keep as an example
// void bsp_UART_1_putchar_it(uint8_t ch);

// Sending a given number of bytes from memory address
void bsp_UART_1_send_buffer(uint8_t* pData, uint16_t size);
// It is called when number of bytes are sent (see above function)
void bsp_UART_1_tx_complete(void);
// Setup UART 1 RX, e.g. set memory location for received byte and enable RX interrupt
void bsp_UART_1_set_en_rx(volatile uint8_t *ch);
// It is called by HAL layer when UART1 receive one byte
void bsp_UART_1_rx_byte_received(void);


///////////////////////////////////////////////////////////////////////////////
// UART 2 provided interface for upper layers
///////////////////////////////////////////////////////////////////////////////
// Sending a given number of bytes from memory address
void bsp_UART_2_send_buffer(uint8_t* pData, uint16_t size);
// It is called when number of bytes are sent (see above function)
void bsp_UART_2_tx_complete(void);
// Setup UART 3 RX, e.g. set memory location for received byte and enable RX interrupt
void bsp_UART_2_set_en_rx(volatile uint8_t *ch);
// It is called by HAL layer when UART3 receives one byte
void bsp_UART_2_rx_byte_received(void);


///////////////////////////////////////////////////////////////////////////////
// SPI 2 provided interface for upper layers
///////////////////////////////////////////////////////////////////////////////
void bsp_SPI_2_send_buffer(uint8_t* const pData, const uint16_t size);


///////////////////////////////////////////////////////////////////////////////
// Free running timer provided interface
///////////////////////////////////////////////////////////////////////////////
// Get timer value in microseconds
uint32_t bsp_Tmr_Get_Val_usec(void);
// Compute diffrence between two timer values
uint32_t bsp_Tmr_Diff(uint32_t initial, uint32_t now);


///////////////////////////////////////////////////////////////////////////////
// RTC
///////////////////////////////////////////////////////////////////////////////

// Call this function in upper layers to get 1 second interrupt (__weak in bsp.c)
void RTC_1_Sec_Interrupt(void);


//==========================================
// BSP provided interfaces
//==========================================
void bsp_Init(void);

#endif
