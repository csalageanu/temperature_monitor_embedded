/**
  ******************************************************************************
  * file    bsp.c
  * date    26-Dec-2017
  ******************************************************************************
  */

#include "bsp.h"

// Project files
#include "eh_error_handler.h"

///////////////////////////////////////////////////////////////////////////////
// Private types definitions
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// Private define
///////////////////////////////////////////////////////////////////////////////
/// SPI2 Maximum Timeout values for flags waiting loops
#define SPI_2_TIMEOUT_MAX                      ((uint32_t)0x1000)


///////////////////////////////////////////////////////////////////////////////
// Private functions protptype
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// Private variables
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// Public functions - implementation
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// Function 
//******************************************************************************
void bsp_Init(void)
{
    // Add initialization code here.
}

//******************************************************************************
// UART 1 putchar() function implementation
//******************************************************************************
//void bsp_UART_1_putchar_it(uint8_t ch)
//{
//    HAL_StatusTypeDef s;
//
//    s = HAL_UART_Transmit_IT(&huart1, &ch, 1);
//
//    if (s != HAL_OK)
//    {
//        eh_Err_Set(ERR_UART_1_TX);
//    }
//}

//******************************************************************************
// UART 1 send buffer function implementation
//******************************************************************************
void bsp_UART_1_send_buffer(uint8_t* pData, uint16_t size)
{
    HAL_StatusTypeDef s;

    /* 17.03.2017 It works with both functions _IT or _DMA. For now DMA is used,
     * Keep it here for reference. In both cases HAL_UART_TxCpltCallback
     * function is called. In case of TX DMA, one more function is generated in
     * ISR file, but in both cases above function is called.
     * This is described in documentation also - HAL UART Generic Driver in
     * UM1725                                                                */
  
//    s = HAL_UART_Transmit_DMA(&huart1, pData, size);
    s = HAL_UART_Transmit_IT(&huart1, pData, size);

    if (s != HAL_OK)
    {
        eh_Err_Set(ERR_UART_1_TX);
    }
}

//******************************************************************************
// UART 2 send buffer function implementation
//******************************************************************************
void bsp_UART_2_send_buffer(uint8_t* pData, uint16_t size)
{
    HAL_StatusTypeDef s;

    s = HAL_UART_Transmit_IT(&huart2, pData, size);

    if (s != HAL_OK)
    {
        eh_Err_Set(ERR_UART_2_TX);
    }
}

//******************************************************************************
// This function should be implemented in upper layers. Does nothing here.
//******************************************************************************
__weak void bsp_UART_1_tx_complete(void)
{
    // Upper layer should implement this function. Do not modify this function.
}

//******************************************************************************
// This function should be implemented in upper layers. Does nothing here.
//******************************************************************************
__weak void bsp_UART_2_tx_complete(void)
{
    // Upper layer should implement this function. Do not modify this function.
}

//******************************************************************************
// Called by HAL platform when ANY serial interface has finished a transmission
//******************************************************************************
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
    // UART 1 transmission complete
    if (huart == &huart1)
    {
        bsp_UART_1_tx_complete();
    }
    else if (huart == &huart2)
    {
        bsp_UART_2_tx_complete();
    }
    else // Unknown UART or tx_complete function not defined for this UART
    {
        eh_Err_Set(ERR_HAL_TX_CPLT_CALLBACK_UNKNOWN_UART);
    }
}

//******************************************************************************
// Called by HAL platform when ANY serial interface report an error
//******************************************************************************
 void HAL_UART_ErrorCallback(UART_HandleTypeDef *UartHandle)
{
    eh_Err_Set(ERR_HAL_UART_ERROR_CALLBACK);
}

//******************************************************************************
// UART 1 reception setup (and enable) - buffer of one byte size
//******************************************************************************
void bsp_UART_1_set_en_rx(volatile uint8_t *ch)
{
    HAL_StatusTypeDef s;

    s = HAL_UART_Receive_IT(&huart1,(uint8_t *)ch, 1);

    if (s != HAL_OK)
    {
        eh_Err_Set(ERR_UART_1_INITIAL_SETUP_FAIL);
    }
}

//******************************************************************************
// UART 2 reception setup (and enable) - buffer of one byte size
//******************************************************************************
void bsp_UART_2_set_en_rx(volatile uint8_t *ch)
{
    HAL_StatusTypeDef s;

    s = HAL_UART_Receive_IT(&huart2,(uint8_t *)ch, 1);

    if (s != HAL_OK)
    {
        eh_Err_Set(ERR_UART_2_INITIAL_SETUP_FAIL);
    }
}

//******************************************************************************
// Called by HAL platform when ANY serial interface receives one byte.
// See UART setup function
//******************************************************************************
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    // UART 1
    if (huart == &huart1)
    {
        bsp_UART_1_rx_byte_received();
    }
    // UART 2
    else if (huart == &huart2)
    {
        bsp_UART_2_rx_byte_received();
    }
    // This UART is not configured
    else
    {
        eh_Err_Set(ERR_HAL_RX_CPLT_CALLBACK_UNKNOWN_UART);
    }
}

//******************************************************************************
// This function should be implemented in upper layers. Does nothing here.
// UART 1 character reception notification
//******************************************************************************
__weak void bsp_UART_1_rx_byte_received(void)
{
    // Upper layer should implement this function. Do not modify this function.
}

//******************************************************************************
// This function should be implemented in upper layers. Does nothing here.
// UART 2 character reception notification
//******************************************************************************
__weak void bsp_UART_2_rx_byte_received(void)
{
    // Upper layer should implement this function. Do not modify this function.
}

//******************************************************************************
// Free running timer - API to get current value
//******************************************************************************
uint32_t bsp_Tmr_Get_Val_usec(void)
{
    return __HAL_TIM_GET_COUNTER(&htim2);
}

//******************************************************************************
// Free running timer - helper function to compute the difference between two
// values
//******************************************************************************
uint32_t bsp_Tmr_Diff(uint32_t initial, uint32_t now)
{
    if (now >= initial)
    {
        return (now - initial);
    }
    else
    {
        return (initial - now);
    }
}

//******************************************************************************
// Driver calls microcontroller library (STM32 HAL) funtion to send on SPI
//******************************************************************************
void bsp_SPI_2_send_buffer(uint8_t* const pData, const uint16_t size)
{
	HAL_StatusTypeDef status;

	status = HAL_SPI_Transmit(&hspi2, pData, size, SPI_2_TIMEOUT_MAX);

	if (status != HAL_OK)
	{
		eh_Err_Set(ERR_SPI_2_TX);
	}	
}


// ###################################################################################
// RTC
// ###################################################################################

//******************************************************************************
// RTC 1 Second interrupt callback
// Upper layer should implement this function. Do not modify this function.
//
//******************************************************************************
__weak void RTC_1_Sec_Interrupt(void)
{
    // Upper layer should implement this function. Do not modify this function.
}

//******************************************************************************
// STM32 HAL - 1 Second interrupt callback from RTC
// 
//******************************************************************************
void HAL_RTCEx_RTCEventCallback(RTC_HandleTypeDef *hrtc)
{
    RTC_1_Sec_Interrupt();
}

//******************************************************************************
// STM32 HAL - 1 Second error callback from RTC
// 
//******************************************************************************
void HAL_RTCEx_RTCEventErrorCallback(RTC_HandleTypeDef *hrtc)
{
    eh_Err_Set(ERR_RTC_1_SECOND);
}



///////////////////////////////////////////////////////////////////////////////
// Private functions - implementation
///////////////////////////////////////////////////////////////////////////////
