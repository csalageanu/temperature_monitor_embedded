/**
  ******************************************************************************
  * file    cbf_circular_buffer.c
  * date    24-April-2017
  ******************************************************************************
  */

#include "cbf_circular_buffer.h"

#include "ms_mcu_specific.h"

///////////////////////////////////////////////////////////////////////////////
// Private types definitions
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// Private define
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// Private functions protptype
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// Private variables
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// Public functions - implementation
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// Initialize an circular buffer structure of uint8_t type
//******************************************************************************
void cbf_u8_Init(volatile struct cbf_u8_buffer* const buffer, volatile uint8_t* const data, const uint8_t size)
{
    ms_disable_irq();
    buffer->data = data;
    buffer->size = size;
    buffer->newest_index = 0;
    buffer->oldest_index = buffer->newest_index;
    ms_enable_irq();
}

//******************************************************************************
// Write one position in an u8 buffer
//******************************************************************************
enum CBF_STATUS cbf_Write(volatile struct cbf_u8_buffer* const buffer, uint8_t byte)
{
    uint8_t next_index = 0;
    uint8_t tmp_old_index = 0;

    ms_disable_irq();
    next_index = ( ((buffer->newest_index) +1) % (buffer->size) );
    tmp_old_index = buffer->oldest_index;
    ms_enable_irq();

    if (next_index == tmp_old_index)
    {
        return BUFFER_FULL;
    }

    // Write data to buffer
    buffer->data[buffer->newest_index] = byte;

    // Update write index
    ms_disable_irq();
    buffer->newest_index = next_index;
    ms_enable_irq();

    return OPERATION_SUCCESSFUL;
}

//******************************************************************************
// Read one position from an u8 buffer
//******************************************************************************
enum CBF_STATUS cbf_Read(volatile struct cbf_u8_buffer* const buffer, uint8_t* const byte)
{
    enum CBF_STATUS ret_r = OPERATION_SUCCESSFUL;

    ms_disable_irq();
    if (buffer->newest_index == buffer->oldest_index)
    {
        ret_r = BUFFER_EMPTY;
    }
    else
    {
        *byte = buffer->data[buffer->oldest_index];
        buffer->oldest_index = ( ((buffer->oldest_index) + 1) % buffer->size );
    }
    ms_enable_irq();

    return ret_r;
}


///////////////////////////////////////////////////////////////////////////////
// Private functions - implementation
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// Function 
//******************************************************************************
