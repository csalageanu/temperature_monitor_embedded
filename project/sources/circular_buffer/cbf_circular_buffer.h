/**
  ******************************************************************************
  * file    cbf_circular_buffer.h
  * date    24-April-2017
  ******************************************************************************
  */

#ifndef __CBF_CIRCULAR_BUFFER_H
#define __CBF_CIRCULAR_BUFFER_H

// Circular buffer APIs and data definitions

#include "bsp.h"

#ifdef __cplusplus
 extern "C" {
#endif

///////////////////////////////////////////////////////////////////////////////
// Types definitions
///////////////////////////////////////////////////////////////////////////////

// Buffer Read/Write status
enum CBF_STATUS
{
    OPERATION_SUCCESSFUL,
    BUFFER_EMPTY,
    BUFFER_FULL
};

// Circular Buffer structure
struct cbf_u8_buffer
{
    volatile uint8_t* data;
    uint8_t  size;          // MUST BE POWER OF 2!!!
    uint8_t  newest_index;
    uint8_t  oldest_index;
};


///////////////////////////////////////////////////////////////////////////////
// Circular buffer - provided interface
///////////////////////////////////////////////////////////////////////////////
void cbf_u8_Init(volatile struct cbf_u8_buffer* const buffer, volatile uint8_t* const data, const uint8_t size);

enum CBF_STATUS cbf_Write(volatile struct cbf_u8_buffer* const buffer, uint8_t byte);
enum CBF_STATUS cbf_Read(volatile struct cbf_u8_buffer* const buffer, uint8_t* const byte);

#endif