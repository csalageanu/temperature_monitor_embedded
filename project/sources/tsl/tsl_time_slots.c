/**
  ******************************************************************************
  * file    tsl_time_slots.c
  * date    19-Jan-2018
  ******************************************************************************
  */

#include <stdio.h>

#include "bsp.h"
#include "eh_error_handler.h"
#include "console.h"
#include "temperature_reader_ctrl.h"
#include "display_ctrl.h"
#include "temperature_alarm_ctrl.h"
#include "buzzer_ctrl.h"
#include "configuration_ctrl.h"
#include "relay_output_ctrl.h"
#include "inter_controller_communication_shared_mem.h"

#include "tsl_time_slots.h"


///////////////////////////////////////////////////////////////////////////////
// Private types definitions
///////////////////////////////////////////////////////////////////////////////
enum TSL_CALL_SEQ
{
    INITIAL_CALL_SEQUENCE = 0,
    NORMAL_CALL_SEQUENCE = 1  
};

enum TSL_NR_OF_CALLS
{
    NR_CALLS_INI_SEQ = 9
};

///////////////////////////////////////////////////////////////////////////////
// Private define
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// Private functions protptype
///////////////////////////////////////////////////////////////////////////////
/* Time slot functions */
static void tsl_no_time_slot(void);
static void tsl_10ms_time_slot(void);
static void tsl_20ms_time_slot(void);
static void tsl_40ms_time_slot(void);
static void tsl_80ms_time_slot(void);
/* Slot time functions call sequeces.
 * First 9 calls are different from the normal call sequence.
 * See excel documentation for the call sequence. */
static void tsl_call_sequence_first(uint32_t cnt);
static void tsl_call_sequence_normal(uint32_t *cnt);


///////////////////////////////////////////////////////////////////////////////
// Private variables
///////////////////////////////////////////////////////////////////////////////



// ###################################################################################
// Add application functionality on below time slots
// ###################################################################################

//******************************************************************************
// 5 ms time slot time. Call here all functions that need to run at this
// frequency.
// Atention!
// This function is on ISR context 
//******************************************************************************
void tsl_5ms_time_slot(void)
{
   
}

//******************************************************************************
// No time slot calls. Here calls are as fast as possible. Not possible to
// know the peroid between calls.
// Keep minimal code here, for minimal interference with time slots
//******************************************************************************
void tsl_no_time_slot(void)
{

}

//******************************************************************************
// 10 ms time slot time. Call here all functions that need to run at this
// frequency.
// Atention!
// This time slot calls added with functions called from 5 ms slot time should
// not exceed the base time of 5 ms.
//******************************************************************************
static void tsl_10ms_time_slot(void)
{
    // ICC SM - Inter component communication shared memory module
    icc_sm_Cyclic();    

    // Error handler
    eh_main_fnc();
}

//******************************************************************************
// 20 ms time slot time. Call here all functions that need to run at this
// frequency.
// Atention!
// This time slot calls added with functions called from 5 ms slot time should
// not exceed the base time of 5 ms.
//******************************************************************************
static void tsl_20ms_time_slot(void)
{
    //
}

//******************************************************************************
// 40 ms time slot time. Call here all functions that need to run at this
// frequency.
// Atention!
// This time slot calls added with functions called from 5 ms slot time should
// not exceed the base time of 5 ms.
//******************************************************************************
static void tsl_40ms_time_slot(void)
{
    temperature_reader_controller_cyclic();
}

//******************************************************************************
// 80 ms time slot time. Call here all functions that need to run at this
// frequency.
// Atention!
// This time slot calls added with functions called from 5 ms slot time should
// not exceed the base time of 5 ms.
//******************************************************************************
static void tsl_80ms_time_slot(void)
{
    bps_LED1_Toggle();
    displ_ctrl_Fnc();
    temperature_alarm_controller_Cyclic();
    buzzer_ctrl_Cyclic();
    relay_output_controller_Cyclic();
}

// ###################################################################################
// End of time slots
// ###################################################################################




///////////////////////////////////////////////////////////////////////////////
// Public functions - implementation
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// Init function
// Function to be called from main().
// Call here all components initiaization functions 
//******************************************************************************
void tsl_Init(void)
{
    // ICC SM - Inter component communication shared memory module
    icc_sm_Init();

    // Error handler component - initialization function
    eh_Init();

    // Console initialization
//    cons_Init();
 
    // Display controller initialization function
    displ_ctrl_Init();

    // Temperature Read Controller initialization function
    temperature_reader_Init();

    // 
    temperature_alarm_Init();

    //
    buzzer_ctrl_Init();

    //
    config_Init();

    //
    relay_output_controller_Init();
}

//******************************************************************************
// Main application function
// This function calls time slots function at necessary time. 
// Function is called by main(). 
//******************************************************************************
void tsl_app_main_fnc(void)
{
    static enum TSL_CALL_SEQ call_seq = INITIAL_CALL_SEQUENCE;
    static uint32_t call_cnt_seq_1 = 0;
    static uint32_t call_cnt_seq_n = 0;

    // No time slot calls. Here calls are as fast as possible. Not possible to
    // know the peroid between calls.
    // Keep minimal code here, for minimal interference with time slots
    tsl_no_time_slot();

    /* Call time slot functions */
    if (call_seq == INITIAL_CALL_SEQUENCE) // At the system initialization need to run this call sequence
    {
        call_cnt_seq_1++;
        tsl_call_sequence_first(call_cnt_seq_1);

        if(call_cnt_seq_1 >= NR_CALLS_INI_SEQ)
        {
            call_seq = NORMAL_CALL_SEQUENCE;
        }
    }
    else if (call_seq == NORMAL_CALL_SEQUENCE) // Normal call sequence
    {
        call_cnt_seq_n++;
        tsl_call_sequence_normal(&call_cnt_seq_n);
    }
    else
    {
        //TODO: err_no = ERR_TSL_CALL_SEQUENCE_UNKNOWN;
    }
}


///////////////////////////////////////////////////////////////////////////////
// Private functions - implementation
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// Initial call sequence for time slot functions.
// This sequence is used only for fist calls. After initial sequece is passed,
// normal sequence is used. 
//******************************************************************************
static void tsl_call_sequence_first(uint32_t cnt)
{
    switch (cnt)
    {
        case 1:
            // no call here
        break;

        case 2:
            tsl_10ms_time_slot();
        break;

        case 3:
            tsl_20ms_time_slot();
        break;

        case 4:
            tsl_10ms_time_slot();
        break;

        case 5:
            tsl_40ms_time_slot();
        break;

        case 6:
            tsl_10ms_time_slot();
        break;

        case 7:
            tsl_20ms_time_slot();
        break;

        case 8:
            tsl_10ms_time_slot();
        break;

        case 9:
            tsl_80ms_time_slot();
        break;

        default:
            eh_Err_Set(ERR_TSL_CSF_OUT_OF_RANGE);
        break;
    }
}

//******************************************************************************
// Normal call sequence for time slot functions.
// After initial sequece is passed, normal sequence is used.
//******************************************************************************
static void tsl_call_sequence_normal(uint32_t *cnt)
{
    switch (*cnt)
    {
        case 1:
            tsl_10ms_time_slot();
        break;

        case 2:
            tsl_20ms_time_slot();
        break;

        case 3:
            tsl_10ms_time_slot();
        break;

        case 4:
            tsl_40ms_time_slot();
        break;

        case 5:
            tsl_10ms_time_slot();
        break;

        case 6:
            tsl_20ms_time_slot();
        break;

        case 7:
            tsl_10ms_time_slot();
        break;

        case 8:
            // none
        break;

        case 9:
            tsl_10ms_time_slot();
        break;

        case 10:
            tsl_20ms_time_slot();
        break;

        case 11:
            tsl_10ms_time_slot();
        break;

        case 12:
            tsl_40ms_time_slot();
        break;

        case 13:
            tsl_10ms_time_slot();
        break;

        case 14:
            tsl_20ms_time_slot();
        break;

        case 15:
            tsl_10ms_time_slot();
        break;

        case 16:
            tsl_80ms_time_slot();
            *cnt = 0;
        break;

        default:
            eh_Err_Set(ERR_TSL_CSN_OUT_OF_RANGE);
            *cnt = 0;
        break;
    }
}
