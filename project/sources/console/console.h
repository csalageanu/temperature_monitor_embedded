/**
  ******************************************************************************
  * file    console.h
  * date    24-April-2017
  ******************************************************************************
  */

#ifndef __CONSOLE_H
#define __CONSOLE_H

#include "bsp.h"


#ifdef __cplusplus
 extern "C" {
#endif

///////////////////////////////////////////////////////////////////////////////
// Provided interface
///////////////////////////////////////////////////////////////////////////////
void cons_Init(void);
int cons_puts(const char *s);

#ifdef __cplusplus
}
#endif

#endif  /* __CONSOLE_H */