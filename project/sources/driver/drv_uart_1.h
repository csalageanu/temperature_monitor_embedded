/**
  ******************************************************************************
  * file    drv_uart_1.h
  * date    11-April-2017
  ******************************************************************************
  */

#ifndef __DRV_UART_1_H
#define __DRV_UART_1_H

#include "bsp.h"


#ifdef __cplusplus
 extern "C" {
#endif

///////////////////////////////////////////////////////////////////////////////
// Provided interface
///////////////////////////////////////////////////////////////////////////////
void drv_uart_1_Init(void);
uint8_t drv_uart_1_Get_Byte(uint8_t* ch);
void drv_uart_1_Send_Buffer(uint8_t* pData, uint16_t size);
uint8_t drv_uart_1_Is_Tx_Ready(void);

#ifdef __cplusplus
}
#endif

#endif  /* __DRV_UART_1_H */
