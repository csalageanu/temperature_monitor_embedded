/**
  ******************************************************************************
  * file    bsp.h
  * date    23-Jun-2017
  ******************************************************************************
  */
#ifndef __DRV_SPI_2_H
#define __DRV_SPI_2_H

#include "bsp.h"


#ifdef __cplusplus
 extern "C" {
#endif

///////////////////////////////////////////////////////////////////////////////
// Provided interface
///////////////////////////////////////////////////////////////////////////////
void drv_spi_2_Init(void);
void drv_spi_2_Transmit(uint8_t * const pData, const uint16_t Size);


#ifdef __cplusplus
}
#endif

#endif // __DRV_SPI_2_H