/**
  ******************************************************************************
  * file    drv_uart_3.c
  * date    30-April-2017
  ******************************************************************************
  */

#include "drv_uart_2.h"

#include "string.h"

#include "eh_error_handler.h"
#include "ms_mcu_specific.h"
#include "cbf_circular_buffer.h"


///////////////////////////////////////////////////////////////////////////////
// Private types definitions
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// Private define
///////////////////////////////////////////////////////////////////////////////
#define RX_BUFFER_SIZE 16 // MUST BE POWER OF 2 !!!


///////////////////////////////////////////////////////////////////////////////
// Private functions protptype
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// Private variables
///////////////////////////////////////////////////////////////////////////////
// Buffer structure for characters received
static volatile struct cbf_u8_buffer rx_buffer;
// Array characters used by circular buffer structure. It will be updated by
// circular buffer APIs. 
static volatile uint8_t rx_data[RX_BUFFER_SIZE] = {0};
// UART received byte - it is transfered here by HAL functions
static volatile uint8_t rx_char;

// Reception
volatile enum BSP_UART_STATUS uart_2_status;


///////////////////////////////////////////////////////////////////////////////
// Public functions - implementation
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// UART 2 initialization function 
//******************************************************************************
void drv_uart_2_Init(void)
{
    cbf_u8_Init(&rx_buffer, rx_data, RX_BUFFER_SIZE);
    bsp_UART_2_set_en_rx(&rx_char);
}

//******************************************************************************
// UART 2 - Get one byte from circular buffer
//******************************************************************************
uint8_t drv_uart_2_Get_Byte(uint8_t* ch)
{
    enum CBF_STATUS ret;

    ret = cbf_Read(&rx_buffer, ch);

    if (ret != OPERATION_SUCCESSFUL)
    {
        return 0;
    }
    return 1;
}

//******************************************************************************
// Weak function in BSP module. Implement here to know when one byte was
// received on this UART
//******************************************************************************
void bsp_UART_2_rx_byte_received(void)
{
    enum CBF_STATUS ret;

    // Save received byte
    ret = cbf_Write(&rx_buffer, rx_char);

    if (ret != OPERATION_SUCCESSFUL)
    {
        eh_Err_Set(ERR_UART_2_CBF_WRITE_FAIL);
    }

    // Must reinit the UART. High overhead but possible most safe solution.
    // See HAL_UART_Receive_IT, flags
    bsp_UART_2_set_en_rx(&rx_char);
}

//******************************************************************************
// Weak function in BSP module. Implement here to know when a serial
// transmission has ended
// This function is possible to be called from ISR (ST HAL platform)
//******************************************************************************
void bsp_UART_2_tx_complete(void)
{
    uart_2_status = UART_READY;
    uart_2_drv_tx_done();
}

//******************************************************************************
// Weak function. Implement in upper layer to know when a serial
// transmission has ended
//******************************************************************************
__weak void uart_2_drv_tx_done(void)
{
    // Upper layer should implement this function. Do not modify this function.
}

//******************************************************************************
// UART TX status
//******************************************************************************
uint8_t drv_uart_2_Is_Tx_Ready(void)
{
    enum BSP_UART_STATUS s;

    ms_disable_irq();
    s = uart_2_status;
    ms_enable_irq();
    
    if (s == UART_READY)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

//******************************************************************************
// Driver implementation for sending a buffer of given size
//******************************************************************************
void drv_uart_2_Send_Buffer(uint8_t* pData, uint16_t size)
{
    bsp_UART_2_send_buffer(pData, size);
}


///////////////////////////////////////////////////////////////////////////////
// Private functions - implementation
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// Function 
//******************************************************************************
