/**
  ******************************************************************************
  * file    drv_uart_2.h
  * date    24-April-2017
  ******************************************************************************
  */

#ifndef __DRV_UART_2_H
#define __DRV_UART_2_H

#include "bsp.h"


#ifdef __cplusplus
 extern "C" {
#endif

///////////////////////////////////////////////////////////////////////////////
// Circular buffer - provided interface
///////////////////////////////////////////////////////////////////////////////
void drv_uart_2_Init(void);
uint8_t drv_uart_2_Get_Byte(uint8_t* ch);
void drv_uart_2_Send_Buffer(uint8_t* pData, uint16_t size);
uint8_t drv_uart_2_Is_Tx_Ready(void);

// Implement this function where is needed to know when transmission is ended.
// It is called from UART driver. Could be called in only one place for now.
// Executed on intterupt context!
extern void uart_2_drv_tx_done(void);

#ifdef __cplusplus
}
#endif

#endif  /* __DRV_UART_1_H */
