/**
  ******************************************************************************
  * file    tqs3.h
  * date    26-Dec-2017
  ******************************************************************************
  */

#ifndef __TQS3_H
#define __TQS3_H

#include "bsp.h"
#include "proj_def.h"

#ifdef __cplusplus
 extern "C" {
#endif

// Number of characters - temperature value as array of characters, NULL terminated
//#define TEMPERATURE_VALUE_ARRAY_SIZE 5

// sensor temperature structure
struct tqs3_sensor_t
{
	// temperature sensor address
	uint8_t address;
	//Array of chars, null terminated(C string) - contains temperature read from thermometer
	char str_temperature[TEMPERATURE_VALUE_ARRAY_SIZE];
    // Temperature as int value
    int32_t i_temperature;
};

// Write temperature value 
int8_t tqs3_Write_Temperature_As_String(const char * const str_ptr, struct tqs3_sensor_t * const tqs3);
// Read temperature value
void tqs3_Read_Temperature_As_String(char * const str_ptr, struct tqs3_sensor_t const * const tqs3);
//  Provide temperature value as integer
int32_t tqs3_Read_Temperature_As_Integer(struct tqs3_sensor_t const * const tqs3);

#ifdef __cplusplus
}
#endif

#endif