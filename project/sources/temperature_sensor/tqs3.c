/**
  ******************************************************************************
  * file    proj_def.h
  * date    26-Dec-2017
  ******************************************************************************
  */


#include "tqs3.h"
#include "string.h"
#include "stdlib.h"



///////////////////////////////////////////////////////////////////////////////
// Public functions - implementation
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// Write temperature value
// str_ptr - constant pointer to array from where to copy characters. Array has to be NULL terminated and size is TEMPERATURE_VALUE_ARRAY_SIZE
// tqs3 - constant pointer to structure of tqs3_sensor type
// return: number of chars written, -1 in case of string size different of TEMPERATURE_VALUE_ARRAY_SIZE
//******************************************************************************
int8_t tqs3_Write_Temperature_As_String(const char * const str_ptr, struct tqs3_sensor_t * const tqs3)
{
    // Check sting length, return with error if size not TEMPERATURE_VALUE_ARRAY_SIZE
    if (strlen(str_ptr) != TEMPERATURE_VALUE_ARRAY_SIZE-1)
    {
        return -1;
    }

    // Copy from one string to another
    tqs3->str_temperature[0] = str_ptr[0];
    tqs3->str_temperature[1] = str_ptr[1];
    tqs3->str_temperature[2] = str_ptr[2];
    tqs3->str_temperature[3] = str_ptr[3];
    tqs3->str_temperature[4] = str_ptr[4];

    // Convert to integer value
    tqs3->i_temperature = strtol(str_ptr, 0, 10);

    return TEMPERATURE_VALUE_ARRAY_SIZE;
}


//******************************************************************************
// Read temperature value as array of characters with NULL at the end (C string)
// str_ptr - pointer to array where to put the characters. Array has to be NULL terminated and size is TEMPERATURE_VALUE_ARRAY_SIZE
// tqs3 - constant pointer to structure of tqs3_sensor type
//******************************************************************************
void tqs3_Read_Temperature_As_String(char * const str_ptr, struct tqs3_sensor_t const * const tqs3)
{
    str_ptr[0] = tqs3->str_temperature[0];
    str_ptr[1] = tqs3->str_temperature[1];
    str_ptr[2] = tqs3->str_temperature[2];
    str_ptr[3] = tqs3->str_temperature[3];
    str_ptr[4] = tqs3->str_temperature[4];
}

//******************************************************************************
// Read temperature value as integer (32 bits)
// tqs3 - constant pointer to structure of tqs3_sensor type
//******************************************************************************
int32_t tqs3_Read_Temperature_As_Integer(struct tqs3_sensor_t const * const tqs3)
{
    return tqs3->i_temperature;
}

