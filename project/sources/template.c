/**
  ******************************************************************************
  * file    template.c
  * date    17-Dec-2017
  ******************************************************************************
  */

#include <stdio.h>



///////////////////////////////////////////////////////////////////////////////
// Private types definitions
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
// Private define
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
// Private functions protptype
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
// Private variables
///////////////////////////////////////////////////////////////////////////////



// ###################################################################################
// Section start
// ###################################################################################


// ###################################################################################
// End of section
// ###################################################################################




///////////////////////////////////////////////////////////////////////////////
// Public functions - implementation
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// Init function
// Function to be called from main().
// 
//******************************************************************************
void template_Init(void)
{

}


///////////////////////////////////////////////////////////////////////////////
// Private functions - implementation
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// Function description
//  
//******************************************************************************
static void template(void)
{

}
