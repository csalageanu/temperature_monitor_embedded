/**
  ******************************************************************************
  * file    eh_error_handler.h
  * date    26-Dec-2017
  ******************************************************************************
  */

#ifndef __EH_ERROR_HANDLER_H
#define __EH_ERROR_HANDLER_H

#include "bsp.h"

#ifdef __cplusplus
 extern "C" {
#endif

///////////////////////////////////////////////////////////////////////////////
// Types definitions
///////////////////////////////////////////////////////////////////////////////

// All application errors
enum ERR_ID
{
    ERR_NO_ERROR = 0,

    // Error in microcontroller library (provided by MCU vendor)
    ERR_MCU_INIT_FAIL,

    // Errors of Time Slot component
    ERR_TSL_CSF_OUT_OF_RANGE,
    ERR_TSL_CSN_OUT_OF_RANGE,

    // HAL UART 
    ERR_HAL_TX_CPLT_CALLBACK_UNKNOWN_UART,
    ERR_HAL_RX_CPLT_CALLBACK_UNKNOWN_UART,
    ERR_HAL_UART_ERROR_CALLBACK,
    
    // UART 1 errors
    ERR_UART_1_TX,
    //ERR_UART_1_TEST_TX_BUSY,
    ERR_UART_1_INITIAL_SETUP_FAIL,
    //ERR_UART_1_ENABLE_RECEPTION_FAIL,
    ERR_UART_1_CBF_WRITE_FAIL,
    //ERR_UART_1_CBF_READ_FAIL,

    // UART 2 errors
    ERR_UART_2_TX,
    ERR_UART_2_INITIAL_SETUP_FAIL,
    ERR_UART_2_CBF_WRITE_FAIL,
    
    // SPI 2 errors
    ERR_SPI_2_TX,

    // RTC errors
    ERR_RTC_1_SECOND,

    // Display (controller and driver)
    ERR_DISPL_CTRL_WRITE_OUTSIDE_OF_DISPLAY_TABLE,
    ERR_DISPL_CTRL_VALUE_MORE_THAN_THREE_DIGITS,
    ERR_DISPL_CTRL_NULL_STRING_SLOT,
    ERR_DISPL_CTRL_SNPRINTF_ERROR,
    ERR_DISPL_CTRL_TQS3_TEMPERATURE_STRING_SIGN_MISSING,
    ERR_DISPL_CTRL_TQS3_STRING_INVALID_SIZE,

    // Temperature read controller
    ERR_T_READ_TQS3_RESPONSE_TOO_MANY_CHARS,
    ERR_T_READ_TQS3_FINAL_MARK_POS_ERROR,
    ERR_T_READ_TQS3_WRONG_PREFIX,
    ERR_T_READ_TQS3_ADDRESS_NO_MATCHING,
    ERR_T_READ_TQS3_CONFIRMATION_ERROR,


    // For debug
    ERR_DBG_BUFFER_OVERFLOW
};

///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
void eh_Init(void);
void eh_main_fnc(void);
void eh_Err_Set(enum ERR_ID err_id);


#endif
