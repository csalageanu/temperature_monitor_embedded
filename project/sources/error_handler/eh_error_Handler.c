/**
  ******************************************************************************
  * file    eh_error_handler.c
  * date    23-Jun-2017
  ******************************************************************************
  */

#include "eh_error_handler.h"
#include "ms_mcu_specific.h"


///////////////////////////////////////////////////////////////////////////////
// Private define
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// Private types definitions
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// Private variables
///////////////////////////////////////////////////////////////////////////////
enum ERR_ID err_no;

///////////////////////////////////////////////////////////////////////////////
// Public functions - implementation
///////////////////////////////////////////////////////////////////////////////

void eh_Init(void)
{
    err_no = ERR_NO_ERROR;
}

//******************************************************************************
// EH module main function. Call from 10ms time slot.
//******************************************************************************
void eh_main_fnc(void)
{
    // Critical errors. Application will be stopped because it is not known how to continue.
    if (  (err_no == ERR_MCU_INIT_FAIL) ||
          (err_no == ERR_TSL_CSF_OUT_OF_RANGE)
       )
    {
        while(1)
        {
            bsp_Error_LED_ON;
            for(uint32_t i=0; i<1000000; i++)
                ;
            bsp_Error_LED_OFF;
            for(uint32_t i=0; i<1000000; i++)
                ;
        }
    }
    // Recovery mecanism exist. Application get into a point where it shouldn't be, but possible to recover (e.g. a default switch case, switch variable is cleared)
    else if (err_no != ERR_NO_ERROR)
    {
        bsp_Error_LED_ON;
    }
    // No error
    else
    {
        bsp_Error_LED_OFF;
    }
}

//******************************************************************************
// Function 
//******************************************************************************
void eh_Err_Set(enum ERR_ID err_id)
{
        ms_disable_irq();
        err_no = err_id;
        ms_enable_irq();
}


///////////////////////////////////////////////////////////////////////////////
// Private functions - implementation
///////////////////////////////////////////////////////////////////////////////
