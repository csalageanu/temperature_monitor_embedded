/**
  ******************************************************************************
  * file    max7219_4dig.h
  * date    24-Jun-2017
  ******************************************************************************
  */
#ifndef __MAX7219_4DIG_H
#define __MAX7219_4DIG_H


#include <stdlib.h>
#include <errno.h>
#include "bsp.h"


#ifdef __cplusplus
 extern "C" {
#endif
	 
/* Exported typedef ----------------------------------------------------------*/


/* Exported define -----------------------------------------------------------*/
#define DISPL_MAX_NR_OF_CHARS                 4   // Max number of characters that display support
#define LAST_CHAR_POS_ON_STRING               (DISPL_MAX_NR_OF_CHARS - 1)
#define POSITION_ON_STRING_FROM_LAST_CHAR(n)  (DISPL_MAX_NR_OF_CHARS - 1 - (n))
	 
	 
///////////////////////////////////////////////////////////////////////////////
// Provided interface
///////////////////////////////////////////////////////////////////////////////
void displ_drv_Init(void);
int32_t displ_drv_Write(const char *str, size_t size);
void displ_drv_DisplayNormalOperation(void);
void displ_drv_DisplayShutdownMode(void);	 

//void max7219_4dig_Init(void);
//void max7219_4dig_Transmit(uint8_t * const pData, const uint16_t Size);


#ifdef __cplusplus
}
#endif

#endif // __DRV_SPI_2_H