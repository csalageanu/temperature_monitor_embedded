/* Includes ------------------------------------------------------------------*/
#include "max7219_4dig.h"


/* Private typedef -----------------------------------------------------------*/
typedef enum _ChipSelectState
{
	CS_Low,
	CS_High
} ChipSelectState;

/* Private define ------------------------------------------------------------*/
#define LED_DISPL_DOT_ON         0x80 // Represent the DOT location on a LED digit


/* Private macro -------------------------------------------------------------*/


/* Private variables ---------------------------------------------------------*/
static uint8_t displ_str[DISPL_MAX_NR_OF_CHARS + 1] = { 0 };

/* Private function prototypes -----------------------------------------------*/
static inline void max7219_init(void);
static inline void displ_ChipSelect(ChipSelectState state);
static inline void displ_SendChar(unsigned char c);

/* Exported functions --------------------------------------------------------*/
/**
  * \brief Initialize 7 segment LED display
  * \param None
  * \retval None
  */
void displ_drv_Init(void)
{
	max7219_init();
}

/**
  * \brief Set Normal Operation Mode for LED display (4 digits, 7 segments)
  * \param None
  * \retval None
  */
void displ_drv_DisplayNormalOperation(void)
{
	// Turn on the display
	displ_ChipSelect(CS_Low);
	displ_SendChar(0x0C);
	displ_SendChar(0x01);
	displ_ChipSelect(CS_High);	
}

/**
  * \brief Set Shutdown Mode for LED display (4 digits, 7 segments)
  * \param None
  * \retval None
  */
void displ_drv_DisplayShutdownMode(void)
{
	// Turn off the display
	displ_ChipSelect(CS_Low);
	displ_SendChar(0x0C);
	displ_SendChar(0x00);
	displ_ChipSelect(CS_High);	
}

/**
  * \brief Write 4 digits to 7 segment LED display
  * \param 1 string to be printed on display
  * \param 2 string size
  * \retval None
  */
int32_t displ_drv_Write(const char *str, size_t size)
{
	size_t cnt = 0;          /* counter used local */
	int32_t retval = 0;      /* Return value of this function */
	uint8_t ch;              /* Temporary buffer, used to display */
	
	// Copy string locally
	for (cnt = 0; cnt < DISPL_MAX_NR_OF_CHARS; cnt++)
	{
		displ_str[cnt] = 0; // Clear entire array
		if (str[cnt] != '\0')
		{
			// Copy characters
			displ_str[cnt] = str[cnt];
		}
	}
	retval = cnt; // Return number of characters copied

	// DIGIT_1 = displ_str[3]
	// DIGIT_2 = displ_str[2]
	// DIGIT_3 = displ_str[1]
	// above is in For loop
	// DIGIT_3 = displ_str[0]
	
	// Write DIGIT 1 to 3
	for (uint32_t i = 0; i < 3; i++)
	{
		displ_ChipSelect(CS_Low);       // SELECT MAX
		displ_SendChar(i + 1);          // Digit place: 1,2,3 and displ_str[3, 2, 1]
		if (displ_str[LAST_CHAR_POS_ON_STRING - i] == '-')
		{
			displ_SendChar(0x0A);       // Digit VALUE = sign -
		}
		else if (displ_str[LAST_CHAR_POS_ON_STRING - i] == ' ')
		{
			displ_SendChar(0x0F);       // Digit OFF
		}
		else
		{
			displ_SendChar(displ_str[LAST_CHAR_POS_ON_STRING - i]);    // Digit value
		}
		displ_ChipSelect(CS_High);      // DESELECT MAX
	}
	
	// Digit 4 - NODE ID
	displ_ChipSelect(CS_Low);       // SELECT MAX
	displ_SendChar(4);              // Digit PLACE
	displ_SendChar(displ_str[0] | LED_DISPL_DOT_ON);   // Digit VALUE
	displ_ChipSelect(CS_High);      // DESELECT MAX
	
	// Return number of characters copyed into local variable.
	// Those will be displayed
	return retval;
}

/* Private functions ---------------------------------------------------------*/
/**
  * \brief Initialize MAX7219 chip
  * \param None
  * \retval None
  */
static inline void max7219_init(void)
{
	uint8_t ch;
	
	// BCD mode for digit decoding
	displ_ChipSelect(CS_Low);
	displ_SendChar(0x09);
	displ_SendChar(0xFF);
	displ_ChipSelect(CS_High);
	// Segment luminosity intensity
	displ_ChipSelect(CS_Low); 
	displ_SendChar(0x0A);
	displ_SendChar(0x0F);
	displ_ChipSelect(CS_High);
	// Display refresh
	displ_ChipSelect(CS_Low);
	displ_SendChar(0x0B);
	displ_SendChar(0x07);
	displ_ChipSelect(CS_High);
	// Turn OFF the display
	displ_ChipSelect(CS_Low);
	displ_SendChar(0x0C);
	displ_SendChar(0x00);
	displ_ChipSelect(CS_High);
    // No test
	displ_ChipSelect(CS_Low);
	displ_SendChar(0x00);
	displ_SendChar(0xFF);
	displ_ChipSelect(CS_High);
}

/**
  * \brief Controls Chip Select line of MAX7219
  * \param None
  * \retval None
  */
static inline void displ_ChipSelect(ChipSelectState state)
{
	if (state == CS_Low)
	{
		bsp_max7218_CS_Low();
	}
	else if (state == CS_High)
	{
		bsp_max7219_CS_High();
	}
}

/**
  * \brief Send one character on SPI
  * \param None
  * \retval None
  */
static inline void displ_SendChar(unsigned char c)
{
	static unsigned char ch;
	
	ch = c;
	bsp_SPI_2_send_buffer(&ch, 1);
}
