/**
  ******************************************************************************
  * file    proj_def.h
  * date    02-Jul-2017
  ******************************************************************************
  */

#ifndef __PROJ_DEF_H
#define __PROJ_DEF_H

#include "bsp.h"


#ifdef __cplusplus
 extern "C" {
#endif

///////////////////////////////////////////////////////////////////////////////
// Project level definitions
///////////////////////////////////////////////////////////////////////////////

// Define number of temperature sensors
#define NR_OF_TEMPERATURE_SENSORS  2

// Number of characters - temperature value as array of characters, NULL terminated
#define TEMPERATURE_VALUE_ARRAY_SIZE 5

#ifdef __cplusplus
}
#endif

#endif