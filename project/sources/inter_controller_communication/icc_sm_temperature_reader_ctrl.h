/**
  ******************************************************************************
  * file    icc_sm_temperature_reader_ctrl.h
  * date    17-Dec-2017
  ******************************************************************************
  */

#ifndef __ICC_SM_TEMPERATURE_READER_H
#define __ICC_SM_TEMPERATURE_READER_H

#include "bsp.h"
#include "tqs3.h"


#ifdef __cplusplus
 extern "C" {
#endif


uint8_t icc_sm_temperature_reader_ctrl_Write_Temperature_As_String(const uint8_t sensor_addr, const char * const str_ptr);


#ifdef __cplusplus
}
#endif

#endif
