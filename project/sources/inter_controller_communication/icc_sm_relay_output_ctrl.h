/**
  ******************************************************************************
  * file    icc_sm_relay_output_ctrl.h
  * date    18-Jan-2018
  ******************************************************************************
  */

#ifndef __ICC_SM_RELAY_OUTPUT_CTRL_H
#define __ICC_SM_RELAY_OUTPUT_CTRL_H

#include "bsp.h"


#ifdef __cplusplus
extern "C" {
#endif


void icc_sm_relay_output_ctrl_1_Second_notification(void);


#ifdef __cplusplus
}
#endif

#endif
