/**
  ******************************************************************************
  * file    inter_controller_communication_shared_mem.h
  * date    19-Jan-2018
  ******************************************************************************
  */

#ifndef __ICC_CONTROLLER_COMMUNICATION_SHARED_MEM_H
#define __ICC_CONTROLLER_COMMUNICATION_SHARED_MEM_H

#include "bsp.h"


#ifdef __cplusplus
extern "C" {
#endif

void icc_sm_Init(void);
void icc_sm_Cyclic(void);

#ifdef __cplusplus
}
#endif

#endif