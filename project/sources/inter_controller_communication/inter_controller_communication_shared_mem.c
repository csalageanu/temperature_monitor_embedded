/**
  ******************************************************************************
  * file    inter_controller_communication_shared_mem.c
  * date    27-Jan-2018
  ******************************************************************************
  */

#include "proj_def.h"
#include "inter_controller_communication_shared_mem.h"

// Provided interfaces. Functions from below headers are implemented in this module
// and are used by Controllers modules.
#include "icc_sm_temperature_reader_ctrl.h"
#include "icc_sm_temperature_alarm_ctrl.h"
#include "icc_sm_relay_output_ctrl.h"


// Required interfaces. Use interfaces provided by Controllers.
#include "tqs3.h"
#include "buzzer_ctrl.h"
#include "configuration_ctrl.h"


///////////////////////////////////////////////////////////////////////////////
// Private variables
///////////////////////////////////////////////////////////////////////////////

// Shared between ISR context and cyclic thread
static volatile uint32_t seconds_counter;




///////////////////////////////////////////////////////////////////////////////
// TEMPERATURE READER: Provided interface (implementation) and data alocation
///////////////////////////////////////////////////////////////////////////////

// Array of TQS3 sensors (each sensor has an address and value, at this moment)
struct tqs3_sensor_t tqs3_sensors_array[NR_OF_TEMPERATURE_SENSORS] = 
{ 
    {1, {0}, 0},
    {2, {0}, 0},
};

#warning "Number of sensors from system should be limited (e.g 48 sensors)"

//******************************************************************************
// Write temperature value
// sensor_addr - address of the sensor where the value will be written
// str_ptr pointer to array from where to copy characters. Array has to be NULL terminated and size is TEMPERATURE_VALUE_ARRAY_SIZE
//******************************************************************************
uint8_t icc_sm_temperature_reader_ctrl_Write_Temperature_As_String(const uint8_t sensor_addr, const char * const str_ptr)
{
    uint8_t ret_val = 0;

    // Sensor with this address does not exist. Exit
    if (sensor_addr > NR_OF_TEMPERATURE_SENSORS)
    {
        return -1;
    }

    ret_val = tqs3_Write_Temperature_As_String(str_ptr, &tqs3_sensors_array[sensor_addr-1]);

    return ret_val;
}




///////////////////////////////////////////////////////////////////////////////
// ALARM 
///////////////////////////////////////////////////////////////////////////////

//******* Requirend interface *******

//******************************************************************************
// Get temperature value
// sensor_addr - address of the sensor where the value will be provided
// str_ptr pointer to array from where to put characters. Array has to be NULL terminated and size is TEMPERATURE_VALUE_ARRAY_SIZE
//******************************************************************************
uint8_t icc_sm_temperature_alarm_ctrl_Read_Temperature_As_String(const uint8_t sensor_addr, char * const str_ptr)
{
    // Sensor with this address does not exist. Exit
    if(sensor_addr > NR_OF_TEMPERATURE_SENSORS)
    {
        return -1;
    }

    tqs3_Read_Temperature_As_String(str_ptr, &tqs3_sensors_array[sensor_addr - 1]);
    return TEMPERATURE_VALUE_ARRAY_SIZE;
}

//******************************************************************************
// Get temperature value as Integer
// sensor_addr - address of the sensor where the value will be provided
// str_ptr pointer to array from where to put characters. Array has to be NULL terminated and size is TEMPERATURE_VALUE_ARRAY_SIZE
//******************************************************************************
int32_t icc_sm_temperature_alarm_ctrl_Read_Temperature_As_Integer(const uint8_t sensor_addr)
{
    // Sensor with this address does not exist. Exit
    if(sensor_addr > NR_OF_TEMPERATURE_SENSORS)
    {
        return -1;
    }

    return tqs3_Read_Temperature_As_Integer(&tqs3_sensors_array[sensor_addr - 1]);
}

//******************************************************************************
// Get temperature low limit
//******************************************************************************
int32_t icc_sm_temperature_alarm_ctrl_Get_Temp_Low_Limit(void)
{
    return config_Get_Temp_Low_Limit();
}

//******************************************************************************
// Get temperature high limit 
//******************************************************************************
int32_t icc_sm_temperature_alarm_ctrl_Get_Temp_High_Limit(void)
{
    return config_Get_Temp_High_Limit();
}



///////////////////////////////////////////////////////////////////////////////
// BUZZER: Implement provided interface for Buzzer (Use interfaces to send commads to Buzzer)
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// Start Temperature too HIGH Alarm
//******************************************************************************
void icc_sm_temperature_alarm_ctrl_Over_Temperature_Alarm(void)
{
    buzzer_ctrl_Sound_Request(BUZZER_SOUND_1);
}

//******************************************************************************
// Start Temperature too LOW Alarm
//******************************************************************************
void icc_sm_temperature_alarm_ctrl_Low_Temperature_Alarm(void)
{
    buzzer_ctrl_Sound_Request(BUZZER_SOUND_2);
}

//******************************************************************************
// Temperature Monitoring function is Disabled
//******************************************************************************
void icc_sm_temperature_alarm_ctrl_Disabled_Temperature_Monitor_Alarm(void)
{
    buzzer_ctrl_Sound_Request(BUZZER_SOUND_3);
}


//******************************************************************************
// Stop temperature alarm (temperature is in range)
//******************************************************************************
void icc_sm_temperature_alarm_ctrl_Temperature_Alarm_Stop(void)
{
    buzzer_ctrl_Sound_Request(BUZZER_STOP);
}




///////////////////////////////////////////////////////////////////////////////
// BSP
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// 1 second interrupt callback. Called from BSP
//******************************************************************************
void RTC_1_Sec_Interrupt(void)
{
    // Shared between cyclic function and ISR context
    seconds_counter++;
}


///////////////////////////////////////////////////////////////////////////////
// ICC SM functions
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// Init function
//******************************************************************************
void icc_sm_Init(void)
{
    seconds_counter = 0;
}

//******************************************************************************
// Cyclic function
//******************************************************************************
void icc_sm_Cyclic(void)
{
    // Shared variable between ISR context and cyclic thread
    if (seconds_counter >= 1)
    {
        seconds_counter = 0;
        
        // Call (notify) necessary components here
        icc_sm_relay_output_ctrl_1_Second_notification();
    }
}
