/**
  ******************************************************************************
  * file    temperature_alarm_ctrl.h
  * date    27-Jan-2018
  ******************************************************************************
  */

#ifndef __ICC_SM_TEMPERATURE_ALARM_H
#define __ICC_SM_TEMPERATURE_ALARM_H

#include "bsp.h"


#ifdef __cplusplus
extern "C" {
#endif

/* Get temperatures from sensors */
int32_t icc_sm_temperature_alarm_ctrl_Read_Temperature_As_Integer(const uint8_t sensor_addr);
int32_t icc_sm_temperature_alarm_ctrl_Get_Temp_Low_Limit(void);
int32_t icc_sm_temperature_alarm_ctrl_Get_Temp_High_Limit(void);

/* Start/stop temperture alarm */
void icc_sm_temperature_alarm_ctrl_Over_Temperature_Alarm(void);
void icc_sm_temperature_alarm_ctrl_Low_Temperature_Alarm(void);
void icc_sm_temperature_alarm_ctrl_Disabled_Temperature_Monitor_Alarm(void);
void icc_sm_temperature_alarm_ctrl_Temperature_Alarm_Stop(void);

#ifdef __cplusplus
}
#endif

#endif
