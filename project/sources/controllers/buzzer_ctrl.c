/**
  ******************************************************************************
  * file    buzzer_ctrl.c
  * date    27-Jan-2018
  ******************************************************************************
  */

/*
#
# Signal(PWM) for buzzer control.
# When signal is HIGH, buzzer will generate sound.
# For example if signal period is 1 second and duty cycle 50 % then the buzzer
# will sound for 0.5 seconds and will be OFF for 0.5 seconds then is repeating
#
# An counter is incremented at 80ms(task reccurence).Signal is generated based
# on this clock. Below counts is reffering to this counter value.
#


# INPUTS :
# Period of PWM signal in seconds
    pwm_period = 0.5
#PWM duty cycle, in %
    duty_cycle = 25
# PWM clock in seconds(PWM is based on this clock input)
    pwm_clock = 0.08


# PWM frequency(not used, just calculated)
    pwm_freq = 1 / pwm_period

# Signal ON time, in seconds
    Ton = (duty_cycle / 100) * pwm_period
# Signal OFF time, in seconds
    Toff = pwm_period - Ton

# Signal ON time, in counts
    Ton_counts = round(Ton / pwm_clock)
# Signal OFF time, in counts
    Toff_counts = round(Toff / pwm_clock)

Above formulas are used to calculate values for SOUND 1 and SOUND 2

*/


#include "buzzer_ctrl.h"



///////////////////////////////////////////////////////////////////////////////
// Private types definitions
///////////////////////////////////////////////////////////////////////////////

// State machine type
typedef enum _buzzer_ctrl_State_t
{
    IDLE,
    OUTPUT_HIGH,
    OUTPUT_HIGH_CONTINUOUS,
    OUTPUT_LOW    
} buzzer_ctrl_State_t;

enum
{
    // SOUND 1
    SOUND_1_T_ON_CNT = 12,
    SOUND_1_T_OFF_CNT = 12,
    // SOUND 2
    SOUND_2_T_ON_CNT = 2,
    SOUND_2_T_OFF_CNT = 5,
    // SOUND 3
    SOUND_3_T_ON_CNT = 1,
    SOUND_3_T_OFF_CNT = 750*60
} BUZZER_SOUNDS;

/*
 *  SOUND 1: Period = 2 sec, duty cycle = 50% (1 second HIGH, 1 second LOW)
 *  SOUND 2: Period = 0.5 sec, duty cycle = 25% (0.125 second HIGH, 0.375 second LOW) (TODO: Check, is this right ?)
 *  SOUND 3: 00.8 sec ON, 3600 sec OFF (0.08*1=0.08sec, 0.08*750*60=60sec*60=3600sec)
*/

///////////////////////////////////////////////////////////////////////////////
// Private define
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
// Private functions protptype
///////////////////////////////////////////////////////////////////////////////
static bool Counts_Loader(buzzer_ctrl_request_t req, uint32_t * const pwm_cnt, bool ON_OFF_time);


///////////////////////////////////////////////////////////////////////////////
// Private variables
///////////////////////////////////////////////////////////////////////////////
/* Memorize external requests */ 
static buzzer_ctrl_request_t buzzer_request, old_request;
static bool new_request;
/* Counts number of pwm clock input (recurrences). No overflow check! */
uint32_t pwm_clock_counter;


// ###################################################################################
// Section start
// ###################################################################################


// ###################################################################################
// End of section
// ###################################################################################




///////////////////////////////////////////////////////////////////////////////
// Public functions - implementation
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// Init function
// 
//******************************************************************************
void buzzer_ctrl_Init(void)
{
    buzzer_request = BUZZER_STOP;
    old_request = buzzer_request;
    pwm_clock_counter = 0;
    new_request = 0;
}

//******************************************************************************
// Function to be called cyclicaly.
// PWM clock
//******************************************************************************
void buzzer_ctrl_Cyclic(void)
{
    /* State machine state */ 
    static buzzer_ctrl_State_t bz_state = IDLE;

    // Clock counter for PWM. Decrement if positive.
    if(pwm_clock_counter > 0)
    {
        pwm_clock_counter--;
    }

    switch (bz_state)
    {
    case IDLE:
        bsp_DO_Buzzer_Off();
        pwm_clock_counter = 0;

        // New buzzer request
        if (buzzer_request != BUZZER_STOP)
        {
            new_request = 0;
            // Move output HIGH
            bz_state = OUTPUT_HIGH;

            if ((buzzer_request == BUZZER_SOUND_1) || (buzzer_request == BUZZER_SOUND_2) || (buzzer_request == BUZZER_SOUND_3))
            {
                // Load counts number
                Counts_Loader(buzzer_request, &pwm_clock_counter, 1);
            }
            else if (buzzer_request == BUZZER_ON)
            {
                // Move to HIGH CONTINUOUS state
                bz_state = OUTPUT_HIGH_CONTINUOUS;
            }
            else
            {
                // Unknown value. Keep IDLE state and clear counter
                pwm_clock_counter = 0;
                bz_state = IDLE;
            }
        }
    break;

    // Output is high, buzzer will sound
    case OUTPUT_HIGH:
        bsp_DO_Buzzer_On();

        if ((buzzer_request == BUZZER_STOP) || (new_request))
        {
            bz_state = IDLE;
        }
        else
        {
            if (pwm_clock_counter == 0)
            {
                bool ret = 0;
                bz_state = OUTPUT_LOW;
                ret = Counts_Loader(buzzer_request, &pwm_clock_counter, 0);
                
                if (0 == ret) // Load counts failed
                {
                    bz_state = IDLE;
                }

            }
        }
    break;

    // Output is low, no buzzer sound
    case OUTPUT_LOW:
        bsp_DO_Buzzer_Off();

        if ((buzzer_request == BUZZER_STOP) || (new_request))
        {
            bz_state = IDLE;
        }
        else
        {
            if (pwm_clock_counter == 0)
            {
                bool ret = 0;
                bz_state = OUTPUT_HIGH;
                ret = Counts_Loader(buzzer_request, &pwm_clock_counter, 1);
                
                if (0 == ret) // Load counts failed
                {
                    bz_state = IDLE;
                }
            }
        }
    break;

    case OUTPUT_HIGH_CONTINUOUS:
        bsp_DO_Buzzer_On();

        if ((buzzer_request == BUZZER_STOP)  || (new_request))
        {
            bz_state = IDLE;
        }
    break;
    
    default:
        bz_state = IDLE;
    break;
    }
}

//******************************************************************************
// New request function.
// 
// 
//******************************************************************************
void buzzer_ctrl_Sound_Request(buzzer_ctrl_request_t sound)
{
    if (old_request != sound)
    {
        buzzer_request = sound;
        new_request = 1;
        old_request = buzzer_request;
    }
}

///////////////////////////////////////////////////////////////////////////////
// Private functions - implementation
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// Load timing (counts) for pulses
// req - current buzzer request
// pwm_cnt - const pointer to pwm counter
// ON_OFF_time - load ON time if 1 else load OFF time
// return 1 - when counts can be loaded else 0 (unknown sound) 
//******************************************************************************
static bool Counts_Loader(buzzer_ctrl_request_t req, uint32_t * const pwm_cnt, bool ON_OFF_time)
{
    // Load counts number
    if(req == BUZZER_SOUND_1)
    {
        if (ON_OFF_time == 1)
        {
            // Load counts for HIGH signal duration, sound 1
            *pwm_cnt = SOUND_1_T_ON_CNT;            
        }
        else
        {
            // Load counts for LOW signal duration, sound 1
            *pwm_cnt = SOUND_1_T_OFF_CNT;                        
        }
        return 1;
    }
    else if(req == BUZZER_SOUND_2)
    {
        if (ON_OFF_time == 1)
        {
            // Load counts for HIGH signal duration, sound 2
            *pwm_cnt = SOUND_2_T_ON_CNT;
        }
        else
        {
            // Load counts for LOW signal duration, sound 2
            *pwm_cnt = SOUND_2_T_OFF_CNT;            
        }
        return 1;
    }
    else if(req == BUZZER_SOUND_3)
    {
        if (ON_OFF_time == 1)
        {
            // Load counts for HIGH signal duration, sound 2
            *pwm_cnt = SOUND_3_T_ON_CNT;
        }
        else
        {
            // Load counts for LOW signal duration, sound 2
            *pwm_cnt = SOUND_3_T_OFF_CNT;            
        }
        return 1;
    }
    else
    {
        // Unknown value. Keep IDLE state and clear counter
        *pwm_cnt = 0;
        return 0;
    }
}
