/**
  ******************************************************************************
  * file    display_ctrl.h
  * date    01-Jul-2017
  ******************************************************************************
  */

#include "display_ctrl.h"

///////////////////////////////////////////////////////////////////////////////
// Private types definitions
///////////////////////////////////////////////////////////////////////////////
// Type definition of Display Controller state
typedef enum _displ_ctrl_State_t
{
	DisplInit,
	DisplNormalMode,
	DisplPrintValue,
	DisplKeepValue,
	DisplShutdown
} displ_ctrl_State_t;


///////////////////////////////////////////////////////////////////////////////
// Private define
///////////////////////////////////////////////////////////////////////////////
/* Size of display table (how many slots/sensors should display support) */
#define NUMBER_OF_DISPLAY_SLOTS  3
/* Task recurence in milisecnds */
#define TASK_RECURENCE (uint32_t)100
/* Time to keep a value on display in seconds. Here 5 */
#define VAL_DISPL_TIME (uint32_t)(5*1000/TASK_RECURENCE)


///////////////////////////////////////////////////////////////////////////////
// Private functions protptype
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// Private variables
///////////////////////////////////////////////////////////////////////////////
/* Display controller state */
static displ_ctrl_State_t stCtrlDispl;
static uint32_t val_displ_timer;
/* Two strings - one for each node. This will hold the temperature and node ID */
static char displ_str_1[DISPL_MAX_NR_OF_CHARS + 1] = "1 --";
static char displ_str_2[DISPL_MAX_NR_OF_CHARS + 1] = "2 --";
static char displ_str_E[DISPL_MAX_NR_OF_CHARS + 1] = "+000";
/* Array of pointers to array of characters (null terminated, C strings). Last pointer from table always NULL */
static char (*displ_tbl[NUMBER_OF_DISPLAY_SLOTS])[DISPL_MAX_NR_OF_CHARS + 1] = { &displ_str_1, &displ_str_2, NULL };


///////////////////////////////////////////////////////////////////////////////
// Public functions - implementation
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// Initialize Display Controller
//******************************************************************************
void displ_ctrl_Init(void)
{
	/* Initialize timer */
	val_displ_timer = 0;
	/* Initialize controller state */
	stCtrlDispl = DisplInit;
	/* Initialize display driver */
	displ_drv_Init();
}

//******************************************************************************
// Controller Module Main Function
//******************************************************************************
void displ_ctrl_Fnc(void)
{
	/* A pointer to store adresses from strings Table */
	static char *str;
	/* Pointer in the Table of strings that needs to be shown on display */
	static uint32_t cntTblValues = 0;
	
	switch (stCtrlDispl)
	{
	case DisplInit:
		str = *(displ_tbl[0]); /* Points to first string on the strings Table */
	
		if (displ_tbl[0])
		{
		    // Write to Display (display is still Shutdown at this time)
			displ_drv_Write(str, strlen(str));
			stCtrlDispl = DisplNormalMode;			
		}	
		break;
		
	case DisplNormalMode:
		// Put the Display in Normal Mode Operation (disgits will light on)
		displ_drv_DisplayNormalOperation();
		stCtrlDispl = DisplPrintValue;
		break;
	
	case DisplPrintValue:
		displ_drv_Write(str, strlen(str));
		stCtrlDispl = DisplKeepValue;
		break;
		
	case DisplKeepValue:
		/* Increment counter and check value  */
		val_displ_timer++;
		if (val_displ_timer >= VAL_DISPL_TIME)
		{
			/* Select next string from table (using this counter) */
			cntTblValues++;
			/* Last pointer in the strings Table is NULL, so start again */
			if (displ_tbl[cntTblValues] == NULL)
			{
				cntTblValues = 0;
			}
			/* Select string to be put on display, then go to print state */
			str = *(displ_tbl[cntTblValues]);
			
			val_displ_timer = 0;
			stCtrlDispl = DisplPrintValue;
		}
		break;
		
	case DisplShutdown:
		// Shutdown the display
		displ_drv_DisplayShutdownMode();
		break;
		
	default:
		// Undefined state
		stCtrlDispl = DisplInit;
		break;
	}
}

//******************************************************************************
// Print a number (max 3 digits) on display
// sid = slot ID to be updated, new value for this slot
// newVal = new value to write on display
//******************************************************************************
void displ_ctrl_Print_3dig_Number(const displ_ctrl_SloID sid, const int32_t newVal)
{
	/* Temporary, to convert from int to string. Should not be
	   more that 3 characters (two digits and sign if the case) */
	static char str[DISPL_MAX_NR_OF_CHARS] = { 0 };
	/* Number of characters resulted from conversion */
	int32_t cx;
	/* Index in Display table. Convert from enum to unsigned integer. */
	uint32_t ix = sid;
	
	/* Entries in Display table, not permitted to write outside of the table */
	if (ix > NUMBER_OF_DISPLAY_SLOTS)
	{
		eh_Err_Set(ERR_DISPL_CTRL_WRITE_OUTSIDE_OF_DISPLAY_TABLE);
        return;
	}
	
	cx = snprintf(str, DISPL_MAX_NR_OF_CHARS, "%i", newVal);
	
	//printf("%s\n", (*displ_tbl[0])+2);
	
	/* Don't allow to write more that 3 positions on any string */
	if (cx > 3)
	{
		eh_Err_Set(ERR_DISPL_CTRL_VALUE_MORE_THAN_THREE_DIGITS);
        return;
	}
	
	/* Don't write if no string in this position */
	if ((*displ_tbl[ix]) == NULL)
	{
		eh_Err_Set(ERR_DISPL_CTRL_NULL_STRING_SLOT);
        return;	
	}
	
    /* Write charcters into display table */
	if (cx == 1) // Writing only last char, clear middle two chars, keep first char
	{
		//Clear old value from string
		*((*displ_tbl[ix]) + POSITION_ON_STRING_FROM_LAST_CHAR(2)) = ' ';
		*((*displ_tbl[ix]) + POSITION_ON_STRING_FROM_LAST_CHAR(1)) = ' ';
		// Copy new data
		memcpy(((*displ_tbl[ix]) + LAST_CHAR_POS_ON_STRING), str, cx);
	}
	if (cx == 2) // Writing last two chars, clear second char, keep first char
	{
		//Clear old value from string
		*((*displ_tbl[ix]) + POSITION_ON_STRING_FROM_LAST_CHAR(2)) = ' ';
		// Copy new data
		memcpy(((*displ_tbl[ix]) + POSITION_ON_STRING_FROM_LAST_CHAR(1)), str, cx);
	}
	else if (cx == 3) // Writing last three chars, keep only first char
	{
		memcpy(((*displ_tbl[ix]) + POSITION_ON_STRING_FROM_LAST_CHAR(2)), str, cx);
	}
    else  // cx = 0 or cx < 0
    {
        eh_Err_Set(ERR_DISPL_CTRL_SNPRINTF_ERROR);
    }
}

//******************************************************************************
// Print string returned by TQS3 temperature sensor. This string represents 
// temperature (SPINEL protocol, format 66).
// sid = slot ID to be updated, new value for this slot
// newVal = new value to write on display - TQS3 string representing temperature
// (7 characters starting with + or - sign and ending with the temperature symbol)
//******************************************************************************
void displ_ctrl_Print_TQS3_String(const displ_ctrl_SloID sid, const char * str_tqs3_temp)
{
	/* Temporary, to convert from TQS3 string representing temperature to display string
       Should not be more that 3 characters (two digits and '-' sign if the case) */
	//static char str[DISPL_MAX_NR_OF_CHARS] = { 0 };
	/* Index in Disply table. Convert from enum to unsigned integer. */
	uint32_t ix = sid;
    /* Temporary char */
    char ch = ' ';


	/* Entries in Display table, not permitted to write outside of the table. */
	if (ix > NUMBER_OF_DISPLAY_SLOTS)
	{
		eh_Err_Set(ERR_DISPL_CTRL_WRITE_OUTSIDE_OF_DISPLAY_TABLE);
        return;
	}

	/* Don't write if no string in this position */
	if ((*displ_tbl[ix]) == NULL)
	{
		eh_Err_Set(ERR_DISPL_CTRL_NULL_STRING_SLOT);
        return;	
	}

    /* TQS3 string example: +024.3C 
     * Only firs 4 characters should be passed to this function.
     */
    if (strlen(str_tqs3_temp) != 4)
    {
        eh_Err_Set(ERR_DISPL_CTRL_TQS3_STRING_INVALID_SIZE);
    }

    /* Copy new values from TQS3 temperature string */
    ch = str_tqs3_temp[0];

    if(ch == '-') // minus sign
    {
        *((*displ_tbl[ix]) + POSITION_ON_STRING_FROM_LAST_CHAR(2)) = '-';    // Put minus sign
        *((*displ_tbl[ix]) + POSITION_ON_STRING_FROM_LAST_CHAR(1)) = str_tqs3_temp[2];  // Tens part of temperature
        *((*displ_tbl[ix]) + LAST_CHAR_POS_ON_STRING) = str_tqs3_temp[3]; // Ones part of the temperature
    }
    else if (ch == '+') // plus sign
    {
        *((*displ_tbl[ix]) + POSITION_ON_STRING_FROM_LAST_CHAR(2)) = ' ';    // Nothing. This display could not print '+' sign.
        *((*displ_tbl[ix]) + POSITION_ON_STRING_FROM_LAST_CHAR(1)) = str_tqs3_temp[2];  // Tens part of temperature
        *((*displ_tbl[ix]) + LAST_CHAR_POS_ON_STRING) = str_tqs3_temp[3]; // Ones part of the temperature
    }
    else
    {
        eh_Err_Set(ERR_DISPL_CTRL_TQS3_TEMPERATURE_STRING_SIGN_MISSING);
    }
    

}

//******************************************************************************
// Call this function to show error code on display
// Error will be shown on last slot
//******************************************************************************
void displ_ctrl_ShowErrOnDisplay(void)
{
	// Improvement: Position in String table is harcoded
	displ_tbl[2] = &displ_str_E;
}

//******************************************************************************
// Call this function to hide error code from display
//******************************************************************************
void displ_ctrl_NoErrOnDisplay(void)
{
	// Improvement: See displ_ctrl_ShowErrOnDisplay()
	displ_tbl[2] = NULL;
}

//******************************************************************************
// Non numerical value indication. Use when no valid data is received from sensors.
// sid = In which slot to be shown invalid data
//******************************************************************************
//void displ_ctrl_InvalidVal(displ_ctrl_SloID sid) - see in .h file why this function is commented
//{
//	/* Convert from enum to unsigned integer - index in Strings table */
//	uint32_t ix = sid;
//
//	/* Entries in String table, not permitted to write
//	   outside of the table
//	   TODO: Autmatically get String table entries. */
//	// TODO: Add error handling
//	if (ix > 3)
//	{
//		while (1)
//			;
//	}
//	/* Don't write if no string in this position */
//	if ((*displ_tbl[ix]) == NULL)
//	{
//		while (1)
//			;		
//	}	
//	
//	*((*displ_tbl[ix]) + POSITION_ON_STRING_FROM_LAST_CHAR(1)) = '-';
//	*((*displ_tbl[ix]) + LAST_CHAR_POS_ON_STRING) = '-';
//}

///////////////////////////////////////////////////////////////////////////////
// Private functions - implementation
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// Function 
//******************************************************************************
