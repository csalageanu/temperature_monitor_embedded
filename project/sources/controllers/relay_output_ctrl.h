/**
  ******************************************************************************
  * file    relay_output_ctrl.h
  * date    26-Dec-2017
  ******************************************************************************
  */

#ifndef __RELAY_OUTPUT_CTRL_H
#define __RELAY_OUTPUT_CTRL_H

#include "bsp.h"

#ifdef __cplusplus
 extern "C" {
#endif

void relay_output_controller_Init(void);
void relay_output_controller_Cyclic(void);

#ifdef __cplusplus
}
#endif

#endif