/**
  ******************************************************************************
  * file    temperature_alarm_ctrl.c
  * date    27-Jan-2018
  ******************************************************************************
  */


#include "temperature_alarm_ctrl.h"
#include "icc_sm_temperature_alarm_ctrl.h"




///////////////////////////////////////////////////////////////////////////////
// Private types definitions
///////////////////////////////////////////////////////////////////////////////
// How fast this task function is called in ms
#define TEMPERATURE_ALRAM_TASK_CALL_RECURRENCE 80


///////////////////////////////////////////////////////////////////////////////
// Private define
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
// Private functions protptype
///////////////////////////////////////////////////////////////////////////////
void Alarm_Unknown_Combination(void);
static void Warning_Temperature_Alarm_Is_Disabled(void);


///////////////////////////////////////////////////////////////////////////////
// Private variables
///////////////////////////////////////////////////////////////////////////////

// Temperature read from thermometer
static uint32_t i_temperature_val[NR_OF_TEMPERATURE_SENSORS] = { 0 };


///////////////////////////////////////////////////////////////////////////////
// Public functions - implementation
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// Init function
// 
//******************************************************************************
void temperature_alarm_Init(void)
{
    //Al_Dis_LED_Cnt = 0;
    //Al_Dis_Sound_Cnt = 0;
}


//******************************************************************************
// Cyclic function
// Read temperature values from sensors and trigger alarm if values out of range
// Buzzer output is used for audible alarm 
//
//******************************************************************************
void temperature_alarm_controller_Cyclic()
{
    int32_t max_temp = icc_sm_temperature_alarm_ctrl_Get_Temp_High_Limit();
    int32_t min_temp = icc_sm_temperature_alarm_ctrl_Get_Temp_Low_Limit();

    // Light and sound if temperature range monitoring is disabled
    Warning_Temperature_Alarm_Is_Disabled();

    // Don't read temperatures if temperature range monitoring is disabled
    if (bsp_DI_Temperature_Alarm_Disable_Read())
    {
        return;
    }

    // If Temperature Alarm is enabled
    // Get actual temperatures measured by sensors.
    for (int i = 0; i < NR_OF_TEMPERATURE_SENSORS; i++)
    {
        i_temperature_val[i] = icc_sm_temperature_alarm_ctrl_Read_Temperature_As_Integer(i+1);
    }
#warning "Number of sensors from system should be limited (e.g 48 sensors)"


    /* ATENTION !
        Alarm is based on values from 2 sensors
    */
    // Temperature is in range (min < T1 < max) AND (min < T2 < max)
    if ((i_temperature_val[0] > min_temp) && (i_temperature_val[0] < max_temp) &&
            (i_temperature_val[1] > min_temp) && (i_temperature_val[1] < max_temp))
    {
        icc_sm_temperature_alarm_ctrl_Temperature_Alarm_Stop();
    }
    // At least one sensor shows High temperature
    else if ((i_temperature_val[0] >= max_temp) || (i_temperature_val[1] >= max_temp))
    {
        icc_sm_temperature_alarm_ctrl_Over_Temperature_Alarm();
    }
    // At lest one sensor shows Low temperature
    else if ((i_temperature_val[0] <= min_temp) || (i_temperature_val[1] <= min_temp))
    {
        icc_sm_temperature_alarm_ctrl_Low_Temperature_Alarm();
    }
    // Should not get here
    else
    {
        //Low alarm 4 sec and Over alarm 4 sec
        Alarm_Unknown_Combination();
    }

    /*
        S1 = Range, S2 = Range
        S1 = Range, S2 = Low
        S1 = Range, S2 = High

        S1 = High, S2 = Range
        S1 = High, S2 = Low
        S1 = High, S2 = High

        S1 = Low, S2 = Range
        S1 = Low, S2 = Low
        S1 = Low, S2 = High

        (S1 = Range) AND (S2 = Range) => No Alarm
        (S1 = Low) OR (S2 = Low)      => Alarm Low
        (S1 = High) OR (S2 = High)    => Alarm High
    */
}

///////////////////////////////////////////////////////////////////////////////
// Private functions - implementation
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
//
//
//******************************************************************************
void Alarm_Unknown_Combination(void)
{
    static uint32_t cycle_counter;

    cycle_counter++;

    if (cycle_counter < 40)
    {
        icc_sm_temperature_alarm_ctrl_Over_Temperature_Alarm();
    }
    else
    {
        icc_sm_temperature_alarm_ctrl_Low_Temperature_Alarm();
    }

    if (cycle_counter >= 80)
    {
        cycle_counter = 0;
    }
}

//******************************************************************************
// Blink LED, also some buzzer sound is generated if Temperature Alarm is
// disabled
//******************************************************************************
static void Warning_Temperature_Alarm_Is_Disabled(void)
{
    // Use for LED that blink when temperature alsrm is disabled
    static uint8_t Al_Dis_LED_Cnt = 0;

    if (bsp_DI_Temperature_Alarm_Disable_Read())
    {
        Al_Dis_LED_Cnt++;

        // #### Visual (LED) - temperature out of range alarm is disabled ####
        if (Al_Dis_LED_Cnt >= 8)
        {
            // Restart counter
            Al_Dis_LED_Cnt = 0;
            // Blink LED
            bps_DO_Temperature_Alarm_Disabled_Toggle();
        }

        // #### Buzzer sound - temperature out of range alarm is disabled ####
        icc_sm_temperature_alarm_ctrl_Disabled_Temperature_Monitor_Alarm();
    }
    else // Reset variables
    {
        Al_Dis_LED_Cnt = 0;
        // Clear LED output - is controlled by function Warning_Temperature_Alarm_Is_Disabled()
        bsp_DO_Temperature_Alarm_Disabled_Off();
        // Stop sound notification - not necessary, sound is controlled by function temperature_alarm_controller_Cyclic()
    }
}
