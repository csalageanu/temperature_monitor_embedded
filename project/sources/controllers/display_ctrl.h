/**
  ******************************************************************************
  * file    display_ctrl.h
  * date    01-Jul-2017
  ******************************************************************************
  */

#ifndef __DISPLAY_CTRL_H
#define __DISPLAY_CTRL_H

#include "string.h"
#include "bsp.h"
#include "max7219_4dig.h"
#include "eh_error_handler.h"

#ifdef __cplusplus
 extern "C" {
#endif

/* Exported typedef ----------------------------------------------------------*/
typedef enum _displ_ctrl_SloID
{
	slot_1 = 0,
	slot_2,
	slot_e
} displ_ctrl_SloID;
	 
/* Exported define -----------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
	 
/* Exported function prototypes ----------------------------------------------*/
void displ_ctrl_Init(void);
void displ_ctrl_Fnc(void);
void displ_ctrl_Print_3dig_Number(const displ_ctrl_SloID sid, const int32_t newVal);
void displ_ctrl_Print_TQS3_String(const displ_ctrl_SloID sid, const char * str_tqs3_temp);
void displ_ctrl_ShowErrOnDisplay(void);
void displ_ctrl_NoErrOnDisplay(void);

//void displ_ctrl_InvalidVal(displ_ctrl_SloID sid); - REMOVE this function, see below comment.
// TODO: To print "- -" on disaply (invalid value), use function displ_ctrl_Print_3dig_Number()
//       with parameter newVal = 0xFFFF.

#ifdef __cplusplus
}
#endif

#endif
