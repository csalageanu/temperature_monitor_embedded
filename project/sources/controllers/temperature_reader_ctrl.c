/**
  ******************************************************************************
  * file    temperature_reader_ctrl.h
  * date    05-Jul-2017
  ******************************************************************************
  */

#include "temperature_reader_ctrl.h"

///////////////////////////////////////////////////////////////////////////////
// Private types definitions
///////////////////////////////////////////////////////////////////////////////
// State machine state
enum tr_state
{
    Idle,
    Send_Cmd,
    Wait_1,
    Check_Response
};


///////////////////////////////////////////////////////////////////////////////
// Private define
///////////////////////////////////////////////////////////////////////////////
// How fast this task function is called in ms
#define TASK_CALL_RECURRENCE 40
// Define time interval at which sensors are read (40 sec)
#define SENSORS_READ_INTERVAL (10000/TASK_CALL_RECURRENCE)
// Wait sensor conversion time (at least 700 ms)
#define SENSOR_CONVERSION_TIME (1000/TASK_CALL_RECURRENCE)
// Number of bytes that TQS3 responds when temperature read command is sent
#define SPINEL_66_TR_RESPONSE_LEN 12


///////////////////////////////////////////////////////////////////////////////
// Private functions protptype
///////////////////////////////////////////////////////////////////////////////
// temperature read comand function
static void tr_send_temperature_read_cmd(const uint8_t address);
static void tr_process_sensor_data(char * out_str, uint8_t const * in_str, size_t in_str_max_len, uint8_t sensor_nr);

///////////////////////////////////////////////////////////////////////////////
// Private variables
///////////////////////////////////////////////////////////////////////////////
// Array of bytes that represents Temperature read command
#define CMD_TEMPERATURE_READ 6
static uint8_t cmd_temperature_read[CMD_TEMPERATURE_READ] = {'*', 'B', '1', 'T', 'R', '\r'};
//static struct _cmd_read_temperature cmd_TR = {'*', 'B', '2', 'T', 'R', '\r'};

// A maximum number of bytes that thermometer sends (not real number of bytes, but an array of maximum)
#define SENSOR_DATA_LEN (size_t)16
// Array of bytes used to copy bytes from UART buffer
static uint8_t sensor_data[SENSOR_DATA_LEN] = {0};
// Index in above array
static uint8_t sd_index = 0;

// Thermometer address
static uint8_t thermometer_address;

// Array of chars, null terminated (C string) - contains temperature read from thermometer. Sent to display controller.
static char str_temperature[TEMPERATURE_VALUE_ARRAY_SIZE] = { 0x00 };


///////////////////////////////////////////////////////////////////////////////
// Public functions - implementation
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// Initialization function of temperature read controller
//******************************************************************************
void temperature_reader_Init(void)
{
    thermometer_address = 1;
    drv_uart_1_Init();
}

//******************************************************************************
// Cyclic function of temperature read controller
//******************************************************************************
void temperature_reader_controller_cyclic(void)
{
    // Temperature read controller actual state
    static enum tr_state trc_state = Idle;
    static uint16_t read_period_cnt = 0; // Need to be 0 at start to read the sensors immediately (see IDLE state)
    static uint16_t wait_conversion = SENSOR_CONVERSION_TIME;

    switch (trc_state)
    {
    case Idle:
        if (read_period_cnt > 0)
        {
            read_period_cnt--;
        }
        else
        {
            // Change state when counter is 0
            trc_state = Send_Cmd;
        }
    break;

    case Send_Cmd:
        tr_send_temperature_read_cmd(thermometer_address);
        // Load counter
        read_period_cnt = SENSOR_CONVERSION_TIME;
        trc_state = Wait_1;
    break;

    case Wait_1:
        if (wait_conversion > 0)
        {
            wait_conversion--;
        }
        else
        {
            // Change state when counter is 0
            trc_state = Check_Response;
        }
    break;

    case Check_Response:
    {
        uint8_t ret = 0;
        uint8_t i = 0;

        // Clear reception buffer
        for(i = 0; i < SENSOR_DATA_LEN; i++)
        {
             sensor_data[i] = 0x00;
        }

        // Copy existing bytes from serial buffer into local buffer
        // Max bytes to copy are SENSOR_DATA_LEN
        for(i = 0; i < SENSOR_DATA_LEN; i++)
        {
            // Copy existing bytes from serial buffer into local buffer
            ret = drv_uart_1_Get_Byte(&sensor_data[i]);
            // Stop when no more data in receive buffer
            if(ret == 0)
            {
                break;
            }

            /* For temperature reading command, TQS3 (Spinel protocol format 66)
               will respond with 12 bytes. If not set an error. */
            if (i > SPINEL_66_TR_RESPONSE_LEN)
            {
                eh_Err_Set(ERR_T_READ_TQS3_RESPONSE_TOO_MANY_CHARS);
            }
        }

        // Check data received and build string that will be sent to display controller
        tr_process_sensor_data(str_temperature, sensor_data, SENSOR_DATA_LEN, thermometer_address);

        // Send substring that represents TQS3 temperature to Display Controller
        displ_ctrl_Print_TQS3_String((displ_ctrl_SloID)(thermometer_address-1), str_temperature);

        icc_sm_temperature_reader_ctrl_Write_Temperature_As_String(thermometer_address, str_temperature);

        // Increment sensor address, so next thermometer will be read
        thermometer_address++;
        // Check if thermometer address is greather that max number of sensors. If so start from first adrress and make a pause
        if (thermometer_address > NR_OF_TEMPERATURE_SENSORS)
        {
            thermometer_address = 1;
            // Reload counter
            read_period_cnt = SENSORS_READ_INTERVAL;
        }           
        // Change state
        trc_state = Idle;
    }
    break;

    default:
        trc_state = Idle;
    break;
    } // end switch
}

///////////////////////////////////////////////////////////////////////////////
// Private functions - implementation
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// Send temperature read command function
//******************************************************************************
static void tr_send_temperature_read_cmd(const uint8_t address)
{
    if ( drv_uart_1_Is_Tx_Ready() )
    {
        // Index 2 to of this array is the thermometer address. Need to be ASCII (e.g. '2')
        cmd_temperature_read[2] = address + 48;  // +48 to get ASCII character
        drv_uart_1_Send_Buffer(cmd_temperature_read, CMD_TEMPERATURE_READ);
    }
}

//******************************************************************************
// Process data recived from sensor
// out_str - pointer to string that holds the string representing the temperature
// in_str - pointer to array of chars that holds data received from sensor (raw data)
// in_str_size - size of input string
// sensor_nr - temperature sensor address, sensor which returned data to be processed
//******************************************************************************
static void tr_process_sensor_data(char * out_str, uint8_t const * input, size_t input_max_len, uint8_t sensor_nr)
{
    char ch = -1;
    uint8_t read_err = 0;

    // Check sensor response length
    for(uint8_t i = 0; i < input_max_len; i++)
    {
        // Normally TQS3 sensor responds with 12 bytes
        // Search for final mark 0x0D ('\r')
        if(*(input + i) == 0x0D)
        {
            if (i != 11)
            {
                eh_Err_Set(ERR_T_READ_TQS3_FINAL_MARK_POS_ERROR);
                read_err = 1;
            }
        }
    }

    // Check prefix. Spinel protocol format 66 prefix is "*B"
    if ( (input[0] != '*') || (input[1] != 'B') )
    {
        eh_Err_Set(ERR_T_READ_TQS3_WRONG_PREFIX);
        read_err = 1;
    }

    // Check sensor address
    ch = input[2];
    if ( isdigit(ch) && ((ch - 48) != sensor_nr) )
    {
        eh_Err_Set(ERR_T_READ_TQS3_ADDRESS_NO_MATCHING);
        read_err = 1; 
    }

    // Check confirmation. Spinel protocol format 66 confirmation ok = index 3 is 0
    if (input[3] != '0')
    {
        eh_Err_Set(ERR_T_READ_TQS3_CONFIRMATION_ERROR);
        read_err = 1;
    }

    // Build temperature string (depends if an error was detected or not)
    if(read_err == 0)
    {
        out_str[0] = input[4];
        out_str[1] = input[5];
        out_str[2] = input[6];
        out_str[3] = input[7];
        out_str[4] = 0x00;  // String end in C, NULL character
    }
    else
    {
        out_str[0] = '+';
        out_str[1] = '0';
        out_str[2] = '9';
        out_str[3] = '9';
        out_str[4] = 0x00;  // String end in C, NULL character        
    }
}
