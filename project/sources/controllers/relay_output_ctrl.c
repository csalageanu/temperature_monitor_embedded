/**
  ******************************************************************************
  * file    relay_output_ctrl.c
  * date    27-Jan-2018
  ******************************************************************************
  */

#include "relay_output_ctrl.h"
#include "limits.h"

#include "icc_sm_relay_output_ctrl.h"

///////////////////////////////////////////////////////////////////////////////
// Private types definitions
///////////////////////////////////////////////////////////////////////////////

// Relay state machine states
enum RLY_STATE
{
    ON,
    OFF 
};

// Relay ON/ Relay OFF timings
enum RLY_TIMINGS
{
    ON_TIME = 600,   // seconds
    OFF_TIME = 3000    // seconds
};


///////////////////////////////////////////////////////////////////////////////
// Private define
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// Private functions protptype
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// Private variables
///////////////////////////////////////////////////////////////////////////////
static uint32_t seconds_cnt;


///////////////////////////////////////////////////////////////////////////////
// Public functions - implementation
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// 1 second callback.
//******************************************************************************
void icc_sm_relay_output_ctrl_1_Second_notification(void)
{
    seconds_cnt++;
    if (seconds_cnt >= __UINT32_MAX__)
    {
        seconds_cnt = 0;
    }
}


//******************************************************************************
// Initialization function - relay output controller
//******************************************************************************
void relay_output_controller_Init(void)
{
    seconds_cnt = 0;
}

//******************************************************************************
// Cyclic function - relay output controller
//******************************************************************************
void relay_output_controller_Cyclic(void)
{
    static enum RLY_STATE rly_state = OFF;
    
    switch (rly_state)
    {
    case OFF:
        bsp_DO_Relay_Off();
        // Turn Off relay when time elapsed
        if (seconds_cnt >= OFF_TIME)
        {
            seconds_cnt = 0;
            rly_state = ON;
        }
    break;

    case ON:
        bsp_DO_Relay_On();
        // Turn Off relay when time elapsed
        if (seconds_cnt >= ON_TIME)
        {
            seconds_cnt = 0;
            rly_state = OFF;
        }
    break;

    default:
        // Unknown state, switch to existing one
        rly_state = OFF;
    break;
    }
}


///////////////////////////////////////////////////////////////////////////////
// Private functions - implementation
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// Function 
//******************************************************************************
