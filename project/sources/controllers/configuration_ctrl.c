/**
  ******************************************************************************
  * file    template.c
  * date    26-Dec-2017
  ******************************************************************************
  */

#include "configuration_ctrl.h"


///////////////////////////////////////////////////////////////////////////////
// Private types definitions
///////////////////////////////////////////////////////////////////////////////

// Temperature limits structure
struct config_temperature_alarm_limits_t
{
	// Temperature max limit
    int32_t max;
    // Temperature min limit
    int32_t min;
};



///////////////////////////////////////////////////////////////////////////////
// Private define
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
// Private functions protptype
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
// Private variables
///////////////////////////////////////////////////////////////////////////////
static struct config_temperature_alarm_limits_t config_temperature_alarm_limits;




///////////////////////////////////////////////////////////////////////////////
// Public functions - implementation
///////////////////////////////////////////////////////////////////////////////

//******************************************************************************
// Init function
// Function to be called from main().
// 
//******************************************************************************
void config_Init(void)
{
    config_temperature_alarm_limits.max = 28;
    config_temperature_alarm_limits.min = 10;
}

//******************************************************************************
// Init function
// Function to be called from main().
// 
//******************************************************************************
int32_t config_Get_Temp_Low_Limit(void)
{
    return config_temperature_alarm_limits.min;
}

int32_t config_Get_Temp_High_Limit(void)
{
    return config_temperature_alarm_limits.max;
}


///////////////////////////////////////////////////////////////////////////////
// Private functions - implementation
///////////////////////////////////////////////////////////////////////////////
