/**
  ******************************************************************************
  * file    buzzer_ctrl.h
  * date    27-Jan-2018
  ******************************************************************************
  */

#ifndef __BUZZER_CTRL_H
#define __BUZZER_CTRL_H

#include "bsp.h"


#ifdef __cplusplus
extern "C" {
#endif

// Request type
typedef enum _buzzer_ctrl_request_t
{
    BUZZER_STOP,
    BUZZER_SOUND_1,
    BUZZER_SOUND_2,
    BUZZER_SOUND_3,
    BUZZER_ON
} buzzer_ctrl_request_t;

// Provided interface
void buzzer_ctrl_Init(void);
void buzzer_ctrl_Cyclic(void);
void buzzer_ctrl_Sound_Request(buzzer_ctrl_request_t sound);

#ifdef __cplusplus
}
#endif

#endif
