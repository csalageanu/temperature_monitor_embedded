/**
  ******************************************************************************
  * file    configuration_ctrl.h
  * date    26-Dec-2017
  ******************************************************************************
  */

#ifndef __CONFIGURATION_CTRL_H
#define __CONFIGURATION_CTRL_H

#include "bsp.h"


#ifdef __cplusplus
extern "C" {
#endif

void config_Init(void);

// Provided interface
int32_t config_Get_Temp_Low_Limit(void);
int32_t config_Get_Temp_High_Limit(void);
int32_t config_Get_Temp_Hysteresis(void);

#ifdef __cplusplus
}
#endif

#endif