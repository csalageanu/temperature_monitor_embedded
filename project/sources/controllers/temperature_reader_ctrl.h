/**
  ******************************************************************************
  * file    temperature_reader_ctrl.h
  * date    17-Dec-2017
  ******************************************************************************
  */

#ifndef __TEMPERATURE_READER_CTRL_H
#define __TEMPERATURE_READER_CTRL_H

#include <ctype.h>
#include "proj_def.h"
#include "drv_uart_1.h"
#include "display_ctrl.h"
#include "eh_error_handler.h"
#include "icc_sm_temperature_reader_ctrl.h"


#ifdef __cplusplus
 extern "C" {
#endif

///////////////////////////////////////////////////////////////////////////////
// Provided interface
///////////////////////////////////////////////////////////////////////////////

// Init function
void temperature_reader_Init(void);

// Main controller function
void temperature_reader_controller_cyclic(void);


#ifdef __cplusplus
}
#endif

#endif