/**
  ******************************************************************************
  * file    temperature_alarm_ctrl.h
  * date    17-Dec-2017
  ******************************************************************************
  */

#ifndef __TEMPERATURE_ALARM_H
#define __TEMPERATURE_ALARM_H

#include "bsp.h"
#include "proj_def.h"

#ifdef __cplusplus
extern "C" {
#endif

void temperature_alarm_Init(void);
void temperature_alarm_controller_Cyclic();


#ifdef __cplusplus
}
#endif

#endif
